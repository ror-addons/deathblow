--Written for RoR by Sullemunk, Inspired by Killingblow
--Extremly sloppy coding, but it get's the job done :)
DBsession = {}
Deathblow = {}
BlowDeath = {}
DBSummary = {}
DBProfile = {}
DBSETTINGS = {}
killspree = 0
sessionkillspree = 0

local version = "v 1.6"

PlayerName = wstring.sub( GameData.Player.name,1,-3 )
local Settings
local index
local index2
local temptype
local temptype2
local supresskill = 0
local supressdeath = 0
local supresstext = 0
local showspeach = 0
local Sorting = 0
local class
local tmpTargetData = {}
ListMode = 3


local Iconshow = {}
local GG = Deathblow			
local DB = {}						
DeathblowWin1 = DB
GG.Win1 = DB 							
Deathblow.SummaryWindow = "SummaryWindow"
Deathblow.SettingsWindow = "SettingsWindow"


DB.winRoot = "DeathblowWin1Window"           
DB.winList = DB.winRoot .. "List"                
DB.winTemplate = "DeathblowWin1WindowTemplate"		
DB.listTitle = L"Deathblow"
DB.Listdata = {}			
DBsession.Listdata = {}

Deathblow.StatValues0 = "Deathblownamesvalue"
Deathblow.StatValues1 = "Deathblowkillsvalue"
Deathblow.StatValues2 = "Deathblowdeathsvalue"
Deathblow.StatValues3 = "Deathblowscorevalue"
Deathblow.StatValues4 = "Deathblowcountvalue"
--Deathblow.StatValues5 = "Deathblowrankvalue"
Deathblow.StatValues6 = "Deathblowratiovalue"


DBsession.StatValues0 = "DBsessionnamesvalue"
DBsession.StatValues1 = "DBsessionkillsvalue"
DBsession.StatValues2 = "DBsessiondeathsvalue"
DBsession.StatValues3 = "DBsessionscorevalue"
DBsession.StatValues4 = "DBsessioncountvalue"
DBsession.StatValues5 = "DBsessionratiovalue"

DBsession.killspreeTitle = "DBsessionkillspreeTitle"
DBsession.killspreeValue = "DBsessionkillspreeValue"

DBSummary.TitleValues0 = "DBSummarytitlenamevalue"
DBSummary.TitleValues1 = "DBSummarytitlekillsvalue"
DBSummary.TitleValues2 = "DBSummarytitledeathsvalue"
DBSummary.TitleValues3 = "DBSummarytitlescorevalue"
DBSummary.TitleValues4 = "DBSummarytitleenemiesvalue"
DBSummary.TitleValues5 = "DBSummarytitleratiovalue"
DBSummary.TitleValues6 = "DBSummarytitlekillsvalue2"
DBSummary.TitleValues7 = "DBSummarytitledeathsvalue2"
DBSummary.TitleValues8 = "DBSummarytitlescorevalue2"
DBSummary.TitleValues9 = "DBSummarytitleenemiesvalue2"
DBSummary.TitleValues10 = "DBSummarytitleratiovalue2"
DBSummary.TitleValues11 = "DBSummarySubtitlevalue"
DBSummary.TitleValues12 = "DBSummarySubtitlevalue2"

DBSummary.TitleValues13 = "DBSummarytitleLastKillvalue"
DBSummary.TitleValues14 = "DBSummarytitleLastDeathvalue"
DBSummary.TitleValues15 = "DBSummarySubtitlevalue3"

DBSummary.TitleValues16 = "DBSummaryTitleValues16"
DBSummary.TitleValues17 = "DBSummaryTitleValues17"
DBSummary.TitleValues18 = "DBSummaryTitleValues18"
DBSummary.TitleValues19 = "DBSummaryTitleValues19"
DBSummary.TitleValues20 = "DBSummaryTitleValues20"
DBSummary.TitleValues21 = "DBSummaryTitleValues21"
DBSummary.TitleValues22 = "DBSummaryTitleValues22"
DBSummary.TitleValues23 = "DBSummaryTitleValues23"
DBSummary.TitleValues24 = "DBSummaryTitleValues24"
DBSummary.TitleValues25 = "DBSummaryTitleValues25"
DBSummary.TitleValues26 = "DBSummaryTitleValues26"
DBSummary.TitleValues27 = "DBSummaryTitleValues27"
DBSummary.TitleValues28 = "DBSummaryTitleValues28"
DBSummary.TitleValues29 = "DBSummaryTitleValues29"
DBSummary.TitleValues30 = "DBSummaryTitleValues30"
DBSummary.TitleValues31 = "DBSummaryTitleValues31"
DBSummary.TitleValues32 = "DBSummaryTitleValues32"

--DBsession.StatValues6 = "DBsessionratiovalue"
DBProfile.TitleValues0 = "DBProfiletitlenamevalue"
DBProfile.Title1 = "DBProfileTitle1"
DBProfile.Title2 = "DBProfileTitle2"
DBProfile.Title3 = "DBProfileTitle3"
DBProfile.Title4 = "DBProfileTitle4"
DBProfile.Title5 = "DBProfileTitle5"
DBProfile.Title6 = "DBProfileTitle6"
DBProfile.Title7 = "DBProfileTitle7"
DBProfile.Title8 = "DBProfileTitle8"
DBProfile.Title9 = "DBProfileTitle9"
DBProfile.Title10 = "DBProfileTitle10"
DBProfile.Title11 = "DBProfileTitle11"
DBProfile.Title12 = "DBProfileTitle12"
DBProfile.Title13 = "DBProfileTitle13"
DBProfile.Title14 = "DBProfileTitle14"
DBProfile.Title15 = "DBProfileTitle15"

DBProfile.Title16 = "DBProfileTitle16"
DBProfile.Title17 = "DBProfileTitle17"
DBProfile.Title19 = "DBProfileTitle19" --Profile Damage title
DBProfile.Title20 = "DBProfileTitle20" --Profile Mitigated title
DBProfile.Title21 = "DBProfileTitle21" --Profile Crit title
DBProfile.Title22 = "DBProfileTitle22" --Profile Damage title
DBProfile.Title23 = "DBProfileTitle23" --Profile Mitigated title
DBProfile.Title24 = "DBProfileTitle24" --Profile Crit title
DBProfile.Title25 = "DBProfileTitle25" --Profile Damage title
DBProfile.Title26 = "DBProfileTitle26" --Profile Mitigated title
DBProfile.Title27 = "DBProfileTitle27" --Profile Crit title
DBProfile.Title28 = "DBProfileTitle28" --Profile Damage title
DBProfile.Title29 = "DBProfileTitle29" --Profile Mitigated title
DBProfile.Title30 = "DBProfileTitle30" --Profile Crit title
DBProfile.Title31 = "DBProfileTitle31" --Profile Crit title
DBProfile.Title32 = "DBProfileTitle32" --Profile Crit title




DBProfile.Value2 = "DBProfileValue2"
DBProfile.Value3 = "DBProfileValue3"
DBProfile.Value4 = "DBProfileValue4"
DBProfile.Value5 = "DBProfileValue5"
DBProfile.Value6 = "DBProfileValue6"
DBProfile.Value7 = "DBProfileValue7"
DBProfile.Value8 = "DBProfileValue8"
DBProfile.Value9 = "DBProfileValue9"
DBProfile.Value10 = "DBProfileValue10"
DBProfile.Value11 = "DBProfileValue11"
DBProfile.Value12 = "DBProfileValue12"
DBProfile.Value13 = "DBProfileValue13"
DBProfile.Value16 = "DBProfileValue16"
DBProfile.Value17 = "DBProfileValue17"
DBProfile.Value18 = "DBProfileValue18"
DBProfile.Value19 = "DBProfileValue19"--Profile Damage Value
DBProfile.Value20 = "DBProfileValue20"--Profile Mitigate Value
DBProfile.Value21 = "DBProfileValue21"--Profile Crit Value
DBProfile.Value22 = "DBProfileValue22"--Profile Damage Value
DBProfile.Value23 = "DBProfileValue23"--Profile Mitigate Value
DBProfile.Value24 = "DBProfileValue24"--Profile Crit Value
DBProfile.Value25 = "DBProfileValue25"--Profile Dodge Value
DBProfile.Value26 = "DBProfileValue26"--Profile Block Value
DBProfile.Value27 = "DBProfileValue27"--Profile Disrupt Value
DBProfile.Value28 = "DBProfileValue28"--Profile Parry Value
DBProfile.Value29 = "DBProfileValue29"--Profile Dodge Value
DBProfile.Value30 = "DBProfileValue30"--Profile Block Value
DBProfile.Value31 = "DBProfileValue31"--Profile Disrupt Value
DBProfile.Value32 = "DBProfileValue32"--Profile Parry Value



DBSummary.StatValues0 = "DBVersion"
DBSummary.StatValues1 = "DBSummarystatkillsvalue"
DBSummary.StatValues2 = "DBSummarystatdeathsvalue"
DBSummary.StatValues3 = "DBSummarystatscorevalue"
DBSummary.StatValues4 = "DBSummarystatenemiesvalue"
DBSummary.StatValues5 = "DBSummarystatratiovalue"

DBSummary.StatValues6 = "DBSummarystatkillsvalue2"
DBSummary.StatValues7 = "DBSummarystatdeathsvalue2"
DBSummary.StatValues8 = "DBSummarystatscorevalue2"
DBSummary.StatValues9 = "DBSummarystatenemiesvalue2"
DBSummary.StatValues10 = "DBSummarystatratiovalue2"


DBSummary.StatValues13 = "DBSummarystatLastKillvalue"
DBSummary.StatValues14 = "DBSummarystatLastDeathvalue"
DBSummary.StatValues19 = "TotalDamage" --Total damage
DBSummary.StatValues20 = "TotalMitigated"
DBSummary.StatValues21 = "TotalCrit"
DBSummary.StatValues22 = "TotalDamage2"
DBSummary.StatValues23 = "TotalMitigated2"
DBSummary.StatValues24 = "TotalCrit2"
DBSummary.StatValues25 = "TotalDodge"
DBSummary.StatValues26 = "TotalBlock"
DBSummary.StatValues27 = "TotalDisrupt"
DBSummary.StatValues28 = "TotalParry"
DBSummary.StatValues29 = "TotalDodge2"
DBSummary.StatValues30 = "TotalBlock2"
DBSummary.StatValues31 = "TotalDisrupt2"
DBSummary.StatValues32 = "TotalParry2"




DBSummary.ListTitle1 = "DBSummaryListTitle1"
DBSummary.ListTitle2 = "DBSummaryListTitle2"
DBSummary.ListTitle3 = "DBSummaryListTitle3"
DBSummary.ListTitle4 = "DBSummaryListTitle4"
DBSummary.ListTitle5 = "DBSummaryListTitle5"
DBSummary.ListTitle6 = "DBSummaryListTitle6"
DBSummary.ListTitle7 = "DBSummaryListTitle7"
DBSummary.ListTitle8 = "DBSummaryListTitle8"
DBSummary.ListTitle9 = "DBSummaryListTitle9"
DBSummary.ListTitle10 = "DBSummaryListTitle10"
DBSummary.ListTitle11= "DBSummaryListTitle11"
DBSummary.ListTitle12= "DBSummaryListTitle12"

DBSummary.ListValue1 = "DBSummaryListValue1"
DBSummary.ListValue2 = "DBSummaryListValue2"
DBSummary.ListValue3 = "DBSummaryListValue3"
DBSummary.ListValue4 = "DBSummaryListValue4"
DBSummary.ListValue5 = "DBSummaryListValue5"
DBSummary.ListValue6 = "DBSummaryListValue6"
DBSummary.ListValue7 = "DBSummaryListValue7"
DBSummary.ListValue8 = "DBSummaryListValue8"
DBSummary.ListValue9 = "DBSummaryListValue9"
DBSummary.ListValue10 = "DBSummaryListValue10"
DBSummary.ListValue11 = "DBSummaryListValue11"
DBSummary.ListValue12 = "DBSummaryListValue12"

DBSummary.killspreeTitle = "DBSummarykillspreeTitle"
DBSummary.killspreeValue = "DBSummarykillspreeValue"

--SettingsLabel1 = "Speach Bubble"



local _hookOnHyperLinkLButtonUp


local function OnHyperLinkLButtonUp( linkData, flags, x, y )
	_hookOnHyperLinkLButtonUp( linkData, flags, x, y )
	local ws_targetName, linkCount = wstring.gsub( linkData, L"Profile:", L"" )
	if( linkCount > 0  ) then

--TextLogAddEntry("Chat", 0, L"Linky "..ws_targetName)	

ProfileName = ws_targetName
ListMode = 4
tabNumber = 5
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",true)
WindowSetShowing("DeathblowWin1WindowList",false)
WindowSetShowing("SummaryWindow", false)
WindowSetShowing("ProfileWindow", true)	
WindowSetShowing(DB.winRoot, true)
Deathblow.Display()

	return
	end
end


function Deathblow.LinkKill()
ProfileName = DBSETTINGS.lastkillname
ListMode = 4
tabNumber = 5
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",true)
WindowSetShowing("DeathblowWin1WindowList",false)
WindowSetShowing("SummaryWindow", false)
WindowSetShowing("ProfileWindow", true)	
Deathblow.Display()
end

function Deathblow.LinkDeath()
ProfileName = DBSETTINGS.lastdeathname
ListMode = 4
tabNumber = 5
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",true)
WindowSetShowing("DeathblowWin1WindowList",false)
WindowSetShowing("SummaryWindow", false)
WindowSetShowing("ProfileWindow", true)	

Deathblow.Display()
end



function Deathblow.Initialize()

	_hookOnHyperLinkLButtonUp = EA_ChatWindow.OnHyperLinkLButtonUp
	EA_ChatWindow.OnHyperLinkLButtonUp = OnHyperLinkLButtonUp

	LibSlash.RegisterSlashCmd("db", function(input) Deathblow.Slash(input) end)
	RegisterEventHandler(TextLogGetUpdateEventId("Combat"), "Deathblow.PlayerRenownUpdated");
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "BlowDeath.GetClass")
	
	
	PlaySound(218)
--Create window
	CreateWindow(DB.winRoot, false)
	CreateWindow(Deathblow.SummaryWindow, false)
	CreateWindow(Deathblow.ProfileWindow, false)
	CreateWindow(Deathblow.SettingsWindow, false)	
--	CreateWindow(DeathblowMenuButton, true)

LabelSetText("SettingsLabel0", L"Settings")	
LabelSetText("SettingsLabel1", L"Speach Bubble")
LabelSetText("SettingsLabel2", L"Alerts \(Kills\)")
LabelSetText("SettingsLabel3", L"Alerts \(Deaths\)")
LabelSetText("SettingsLabel4", L"TextLogs")
	LabelSetText(DB.winRoot .. "TitleBarText", DB.listTitle )
	ButtonSetText( DB.winRoot.."NameButton",  L"Name"	)
	ButtonSetText( DB.winRoot.."KillsButton",	L"K")
	ButtonSetText( DB.winRoot.."DeathsButton", L"D")
	ButtonSetText( DB.winRoot.."RankButton", L"R")
	ButtonSetText( DB.winRoot.."ScoreButton", L"Score")
	ButtonSetText( DB.winRoot.."RatioButton", L"Ratio")
	ButtonSetText( DB.winRoot.."IconButton", L"#")
	ButtonSetText(DB.winRoot.."TotalTab", L"Total")
	ButtonSetText(DB.winRoot.."SessionTab", L"Session")
	ButtonSetText(DB.winRoot.."ClassTab", L"Class")
	ButtonSetText(DB.winRoot.."SummTab", L"Overall")
	ButtonSetText(DB.winRoot.."ProfileTab", L"Profile")
	
	ButtonSetText("DBSummarystatLastKillButton", L"")
	ButtonSetText("DBSummarystatLastDeathButton", L"")
	

	UpdateSettings()
	Deathblow.SetupLanguage()


	LayoutEditor.RegisterWindow( "DeathblowWin1Window", L"DeathBlow", L"Deathblow Stats", false, false, true, nil )	
	LayoutEditor.RegisterWindow(
        "DeathblowMenuButton",             -- window name internal
        L"Deathblow Menu Button",              -- window display name
        L"Toggle Deathblow",   -- description
        true, true,                     -- allow resize width, height
        true,                           -- allow hide
        nil)            	
	
	
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",false)	
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",true)


	TextLogAddEntry("Chat", 0, L"<icon=57> DeathBlow "..towstring(version)..L" Loaded. Use /db for commands")
	
if (DBsession.list == nil) then ButtonSetDisabledFlag( "DeathblowWin1WindowSessionTab",true) else  ButtonSetDisabledFlag( "DeathblowWin1WindowSessionTab",false) end
if (ProfileName == nil) then ButtonSetDisabledFlag( "DeathblowWin1WindowProfileTab",true) else ButtonSetDisabledFlag( "DeathblowWin1WindowProfileTab",false) end
if (Deathblow.SavedSettings == nil) then ButtonSetDisabledFlag( "DeathblowWin1WindowSummTab",true); ButtonSetDisabledFlag( "DeathblowWin1WindowTotalTab",true); ButtonSetDisabledFlag( "DeathblowWin1WindowClassTab",true) else
ButtonSetDisabledFlag( "DeathblowWin1WindowSummTab",false); ButtonSetDisabledFlag( "DeathblowWin1WindowTotalTab",false); ButtonSetDisabledFlag( "DeathblowWin1WindowClassTab",false)
end

WindowSetShowing(DB.winRoot, false)	

if (not DBSETTINGS) then DBSETTINGS = { lastdeathname = "", lastdeathtime = "", lastdeathweapon = "", lastdeathclass = "", lastkillname = "", lastkilltime = "", lastkillweapon = "", lastkillclass = "",killspree=0,supresskill = 0,supressdeath=0,supresstext=0,showspeach=0,totalDamageTo=0,totalMitigatedTo=0,totalCritTo=0,totalDodgeTo=0,totalBlockTo=0,totalDisruptTo=0,totalParryTo=0,totalDamageFrom=0,totalMitigatedFrom=0,totalCritFrom=0,totalDodgeFrom=0,totalDisruptFrom=0,totalBlockFrom=0,totalParryFrom=0} end	
if ( not DBSETTINGS.killspree ) or (DBSETTINGS.killspree == nill) then DBSETTINGS.killspree = 0 end
if ( not DBSETTINGS.supresskill ) or (DBSETTINGS.supresskill == nil) then DBSETTINGS.supresskill = 0 end
if ( not DBSETTINGS.supressdeath ) or (DBSETTINGS.supressdeath == nil) then DBSETTINGS.supressdeath = 0 end
if ( not DBSETTINGS.supresstext ) or (DBSETTINGS.supresstext == nil) then DBSETTINGS.supresstext = 0 end
if ( not DBSETTINGS.showspeach ) or (DBSETTINGS.showspeach == nil) then DBSETTINGS.showspeach = 0 end
if ( not DBSETTINGS.totalDamageTo ) or (DBSETTINGS.totalDamageTo == nil) then DBSETTINGS.totalDamageTo = 0 end
if ( not DBSETTINGS.totalMitigatedTo ) or (DBSETTINGS.totalMitigatedTo == nil) then DBSETTINGS.totalMitigatedTo = 0 end
if ( not DBSETTINGS.totalCritTo ) or (DBSETTINGS.totalCritTo == nil) then DBSETTINGS.totalCritTo = 0 end
if ( not DBSETTINGS.totalDamageFrom ) or (DBSETTINGS.totalDamageFrom == nil) then DBSETTINGS.totalDamageFrom = 0 end
if ( not DBSETTINGS.totalMitigatedFrom ) or (DBSETTINGS.totalMitigatedFrom == nil) then DBSETTINGS.totalMitigatedFrom = 0 end
if ( not DBSETTINGS.totalCritFrom ) or (DBSETTINGS.totalCritFrom == nil) then DBSETTINGS.totalCritFrom = 0 end
if ( not DBSETTINGS.totalDodgeTo ) or (DBSETTINGS.totalDodgeTo == nil) then DBSETTINGS.totalDodgeTo = 0 end
if ( not DBSETTINGS.totalDodgeFrom ) or (DBSETTINGS.totalDodgeFrom == nil) then DBSETTINGS.totalDodgeFrom = 0 end
if ( not DBSETTINGS.totalBlockTo ) or (DBSETTINGS.totalBlockTo == nil) then DBSETTINGS.totalBlockTo = 0 end
if ( not DBSETTINGS.totalBlockFrom ) or (DBSETTINGS.totalBlockFrom == nil) then DBSETTINGS.totalBlockFrom = 0 end
if ( not DBSETTINGS.totalDisruptTo ) or (DBSETTINGS.totalDisruptTo == nil) then DBSETTINGS.totalDisruptTo = 0 end
if ( not DBSETTINGS.totalDisruptFrom ) or (DBSETTINGS.totalDisruptFrom == nil) then DBSETTINGS.totalDisruptFrom = 0 end
if ( not DBSETTINGS.totalParryTo ) or (DBSETTINGS.totalParryTo == nil) then DBSETTINGS.totalParryTo = 0 end
if ( not DBSETTINGS.totalParryFrom ) or (DBSETTINGS.totalParryFrom == nil) then DBSETTINGS.totalParryFrom = 0 end


UpdateSettings()



end

	
function GetNumSymbol( value )

	local returnValue
	if ( value > 0 ) then returnValue = "+"..value
	elseif ( value < 0 ) then returnValue = wstring.gsub( towstring( value ), L"-", L"-")	
	else returnValue = value end

	return towstring( returnValue )
end
	
	
	
function Deathblow.Slash(action)	--Slashcommands 
	local input1 = string.sub(action,0,string.find(action," "))
	if string.find(action," ") ~= nil then
		input1 = string.sub(action,0,string.find(action," ")-1)
		input2 = string.sub(action,string.find(action," ")+1,-1)
	end

	if input1 == "class" then BlowDeath.career()
	elseif input1 == "player" then Deathblow.ScoreAll(trim(input2),3)
	elseif input1 == "hidedeath" or input1 == "hd" then DBSETTINGS.supressdeath = 1
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=58> DeathAlerts Hidden")
	elseif input1 == "showdeath" or input1 == "sd" then DBSETTINGS.supressdeath = 0
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=57> DeathAlerts Showing")
	elseif input1 == "hidekill" or input1 == "hk" then DBSETTINGS.supresskill = 1
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=58> KillAlerts Hidden")
	elseif input1 == "showkill" or input1 == "sk" then DBSETTINGS.supresskill = 0
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=57> KillAlerts Showing")
	
	elseif input1 == "hidespeach" or input1 == "hs" then DBSETTINGS.showspeach = 1; UpdateSettings()
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=58> KillSpeachBubble Hidden")
	elseif input1 == "showspeach" or input1 == "ss" then DBSETTINGS.showspeach = 0 ;UpdateSettings()
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=57> KillSpeachBubble Showing")


	elseif input1 == "hidetext" or input1 == "ht" then DBSETTINGS.supresstext = 1
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=58> Text Hidden")
	elseif input1 == "showtext" or input1 == "st" then DBSETTINGS.supresstext = 0
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=57> Text Showing")
	elseif input1 == "s" or input1 == "score" or input1 == "stats" or input1 == "show" then DB_Show()
	elseif input1 == "session" then DBsession.Display()
	elseif input1 == "total" then BlowDeath.total()
	elseif input1 == "list" then BlowDeath.list()
	elseif input1 == "" or input1 == "help" then
	TextLogAddEntry("Chat", 0, L"\n[DeathBlow]: DeathBlow useage:")
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db s ,score ,stats ,show  - will show the Deathblow score window")	
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db total - will print out all stats/scores")
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db session - will print out tats/scores from this session")
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db class - prints out kills/deaths from different classes")
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db player <playername> - will print out <playername> stats/score")
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db hidespeach (hs) and showspeach (ss) - will toggle the KillSpeachBubble off/on")
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db hidekill (hk) and showkill (sk) - will toggle the death alert notify off/on")
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db hidedeath (hd) and showdeath (sd) - will toggle the death alert notify off/on")
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db hidetext (ht) and showtext (st) - will toggle the Textlog off/on")		
	TextLogAddEntry("Chat", 0, L"[DeathBlow]: /db list - prints all players stored in the gathrered database list, !This can be a long list! \n")
	end
Deathblow.Display()
	end

function Deathblow.PlayerRenownUpdated(updateType, filterType)	--The alert system that displays the kills / deaths

	local msg
	local currenttime
	local num = TextLogGetNumEntries("Combat") - 1
    currenttime, _, msg = TextLogGetEntry("Combat", num);    
	--TIMESTAMP
	local hours,mins,secs = currenttime:match(L"([0-9]+):([0-9]+):([0-9]+)")
	local timestamp = (L""..towstring(Calendar.todaysYear)..L"-"..towstring(Calendar.todaysMonth)..L"-"..towstring(Calendar.todaysDay)..L" "..towstring(hours)..L":"..towstring(mins))

if ( updateType ~= SystemData.TextLogUpdate.ADDED ) then 
	return 
end
	
------------------------------------------------------------------	
--===========[CALCULATING DAMAGE/MITIGATION/DEFENSES]=============	
------------------------------------------------------------------	
--Attacking!
if ( filterType == ChatSettings.Channels[1002].id)then

--	if (TargetNisse ~= 6)then

	-- Look for Dodges
	if msg:find(txt_dodged) then 
if Language == 1 then
Nick2,_ = msg:match( L"([%a]+) dodged ([^%.]+)")
elseif Language == 2 then
Nick2,_ = msg:match( L"([%a]+) a esquivé ([^%.]+)")
 elseif Language == 3 then	
Nick2,_ = msg:match( L"([%a]+) ist ([^%.]+) ausgewichen.")	 
elseif Language == 4 then	
Nick2 = msg:match( L"([%a]+) Schivato [^%.]+")
elseif Language == 5 then
Nick2 = msg:match( L"([%a]+) esquivado [^%.]+") 
elseif Language == 10 then
_,Nick2 = msg:match( L"([^%.]+) %(([%a]+)%) уклоняется.")
			 		 
end
		if (Nick2 ~= nil) then 
--		if (TargetNisse == 5) or (TargetNisse == 0) then
--		if not BlowDeath.SavedSettings[tostring(Nick2)] then BlowDeath.ScoreAll(tostring(Nick2),L"<icon=43>",txt_Unknown,L"?",L"1")	end
--		if BlowDeath.SavedSettings[tostring(Nick2)] then
--			TextLogAddEntry("Chat", 11,L"[DB] Debug: DODGE!! "..towstring(Nick2)) 
			if (BlowDeath.SavedSettings[tostring(Nick2)] ~= nil) then
				if not (BlowDeath.SavedSettings[tostring(Nick2)].DodgeTo) or (BlowDeath.SavedSettings[tostring(Nick2)].DodgeTo == nil) then BlowDeath.SavedSettings[tostring(Nick2)].DodgeTo = 0 end
				BlowDeath.SavedSettings[tostring(Nick2)].DodgeTo = BlowDeath.SavedSettings[tostring(Nick2)].DodgeTo+1
--				TextLogAddEntry("Chat", 11,L"[DB] Debug: Dodges: "..BlowDeath.SavedSettings[tostring(Nick2)].DodgeTo) 
				if ( not DBSETTINGS.totalDodgeTo ) or (DBSETTINGS.totalDodgeTo == nil) then DBSETTINGS.totalDodgeTo = 0 end
				DBSETTINGS.totalDodgeTo = DBSETTINGS.totalDodgeTo+1	
			end
			end
--			end
	end
	-- Look for Disrupts hits
	if msg:find(txt_disrupted) then 
if Language == 1 then
Nick2,_ = msg:match(L"([%a]+) disrupted ([^%.]+)")
elseif Language == 2 then
Nick2,_ = msg:match( L"([%a]+) a dissipé ([^%.]+)")
elseif Language == 3 then	
Nick2,_ = msg:match( L"([%a]+) hat ([^%.]+) gestört.")
elseif Language == 4 then	
Nick2 = msg:match( L"([%a]+) interrotto [^%.]+")
elseif Language == 5 then
Nick2 = msg:match( L"([%a]+) trastocado [^%.]+") 
elseif Language == 10 then
_,Nick2 = msg:match( L"([^%.]+) %(([%a]+)%) предотвращает срабатывание.")
			 	 	 
end
	if (Nick2 ~= nil) then 
--			if (TargetNisse == 5) or (TargetNisse == 0) then
--			if not BlowDeath.SavedSettings[tostring(Nick2)] then BlowDeath.ScoreAll(tostring(Nick2),L"<icon=43>",txt_Unknown,L"?",L"1")	end
--			if BlowDeath.SavedSettings[tostring(Nick2)] then
--			TextLogAddEntry("Chat", 11,L"[DB] Debug: DISRUPTED!! "..towstring(Nick2)) 
			if (BlowDeath.SavedSettings[tostring(Nick2)] ~= nil) then
				if not (BlowDeath.SavedSettings[tostring(Nick2)].DisruptTo) or (BlowDeath.SavedSettings[tostring(Nick2)].DisruptTo == nil)  then BlowDeath.SavedSettings[tostring(Nick2)].DisruptTo = 0 end
				BlowDeath.SavedSettings[tostring(Nick2)].DisruptTo = BlowDeath.SavedSettings[tostring(Nick2)].DisruptTo+1
--				TextLogAddEntry("Chat", 11,L"[DB] Debug: "..towstring(Nick2)..L" Disrupts: "..BlowDeath.SavedSettings[tostring(Nick2)].DisruptTo) 
				if ( not DBSETTINGS.totalDisruptTo ) or (DBSETTINGS.totalDisruptTo == nil) then DBSETTINGS.totalDisruptTo = 0 end
				DBSETTINGS.totalDisruptTo = DBSETTINGS.totalDisruptTo+1	
			end
			end
--			end
	end
	-- Look for Blocks
	if msg:find(txt_blocked) then 
if Language == 1 then
Nick2,_ = msg:match( L"([%a]+) blocked ([^%.]+)")
elseif Language == 2 then
Nick2,_ = msg:match( L"([%a]+) a bloqué ([^%.]+)")	
elseif Language == 3 then	
Nick2,_ = msg:match( L"([%a]+) hat ([^%.]+) geblockt.")
elseif Language == 4 then	
Nick2 = msg:match( L"([%a]+) Bloccato [^%.]+")
elseif Language == 5 then
Nick2 = msg:match( L"([%a]+) bloqueado [^%.]+") 
elseif Language == 10 then
_,Nick2 = msg:match( L"([^%.]+) %(([%a]+)%) ставит блок.")
			 		 			 	 
end
	if (Nick2 ~= nil) then 
--		if (TargetNisse == 5) or (TargetNisse == 0) then
--		if not BlowDeath.SavedSettings[tostring(Nick2)] then BlowDeath.ScoreAll(tostring(Nick2),L"<icon=43>",txt_Unknown,L"?",L"1")	end		
--	if BlowDeath.SavedSettings[tostring(Nick2)] then
--			TextLogAddEntry("Chat", 11,L"[DB] Debug: BLOCKED!! "..towstring(Nick2)) 
			if (BlowDeath.SavedSettings[tostring(Nick2)] ~= nil) then
				if not (BlowDeath.SavedSettings[tostring(Nick2)].BlockTo) or (BlowDeath.SavedSettings[tostring(Nick2)].BlockTo == nil) then BlowDeath.SavedSettings[tostring(Nick2)].BlockTo = 0 end
				BlowDeath.SavedSettings[tostring(Nick2)].BlockTo = BlowDeath.SavedSettings[tostring(Nick2)].BlockTo+1
--				TextLogAddEntry("Chat", 11,L"[DB] Debug: "..towstring(Nick2)..L" Blocks: "..BlowDeath.SavedSettings[tostring(Nick2)].BlockTo)
				if ( not DBSETTINGS.totalBlockTo ) or (DBSETTINGS.totalBlockTo == nil) then DBSETTINGS.totalBlockTo = 0 end
				DBSETTINGS.totalBlockTo = DBSETTINGS.totalBlockTo+1	
			end
			end
--			end
	end
	-- Look for Parries
	if msg:find(txt_parried) then 
if Language == 1 then
Nick2,_ = msg:match( L"([%a]+) parried ([^%.]+)")
elseif Language == 2 then
Nick2,_ = msg:match( L"([%a]+) a paré ([^%.]+)")
elseif Language == 3 then	
Nick2,_ = msg:match( L"([%a]+) hat ([^%.]+) pariert.")
elseif Language == 4 then	
Nick2 = msg:match( L"([%a]+) parato [^%.]+")
elseif Language == 5 then
Nick2 = msg:match( L"([%a]+) parar [^%.]+") 
elseif Language == 10 then
_,Nick2 = msg:match( L"([^%.]+) %(([%a]+)%) парирует.")
			 		 			 	 			 
end
	 if (Nick2 ~= nil) then 
--		if (TargetNisse == 5) or (TargetNisse == 0) then
--		if not BlowDeath.SavedSettings[tostring(Nick2)] then BlowDeath.ScoreAll(tostring(Nick2),L"<icon=43>",txt_Unknown,L"?",L"1")	end
--	if BlowDeath.SavedSettings[tostring(Nick2)] then
--			TextLogAddEntry("Chat", 11,L"[DB] Debug: PARRY!! "..towstring(Nick2)) 
			if (BlowDeath.SavedSettings[tostring(Nick2)] ~= nil) then
				if not (BlowDeath.SavedSettings[tostring(Nick2)].ParryTo) or (BlowDeath.SavedSettings[tostring(Nick2)].ParryTo == nil) then BlowDeath.SavedSettings[tostring(Nick2)].ParryTo = 0 end
				BlowDeath.SavedSettings[tostring(Nick2)].ParryTo = BlowDeath.SavedSettings[tostring(Nick2)].ParryTo+1
--				TextLogAddEntry("Chat", 11,L"[DB] Debug: "..towstring(Nick2)..L" Parrys: "..BlowDeath.SavedSettings[tostring(Nick2)].ParryTo)
				if ( not DBSETTINGS.totalParryTo ) or (DBSETTINGS.totalParryTo == nil) then DBSETTINGS.totalParryTo = 0 end
				DBSETTINGS.totalParryTo = DBSETTINGS.totalParryTo+1	
			end	
			end
--			end
	end
	
	
	-- Dealing Damage and Mitigation	
if Language ==1 then
_,Nick,Damage,Mitigated = msg:match( L"Your ([^%.]+) hits ([%a]+) for (%d+) damage. %((%d+) mitigated%)")		
elseif Language == 2 then
_,_,Nick,Damage,Mitigated = msg:match( L"([^%.]+) inflige ([^%.]+) à ([%a]+) qui subit (%d+) points de dégâts. %((%d+) atténué%)")	
elseif Language == 3 then
Nick,_,_,Damage,Mitigated = msg:match( L"Ihr trefft ([%a]+) ([^%.]+) Euer ([^%.]+) verursacht (%d+) Schadenspunkte. %(Um (%d+) Punkte abgeschwächt%)")
elseif Language == 4 then
_,Damage,Nick,Mitigated = msg:match( L"Il tuo ([^%.]+) infligge (%d+) da[^%.]+ a ([%a]+).%((%d+) mitigati%)")
elseif Language == 5 then
Nick,Damage,Mitigated = msg:match( L"Tu [^%.]+ gol[^%.]+ a ([%a]+) causando (%d+) de daño. %((%d+) mitigado%)")	
elseif Language == 10 then
_,Nick,Damage,_,Mitigated = msg:match( L"([^%.]+) %(([%a]+)%) (%d+) ед. ([^%.]+).%(ослаблено: (%d+)%)")
end
	if (Nick ~= nil) then 
--		if not BlowDeath.SavedSettings[tostring(Nick)] then BlowDeath.ScoreAll(tostring(Nick),L"<icon=43>",txt_Unknown,L"?",L"1")	end
		if (BlowDeath.SavedSettings[tostring(Nick)] ~= nil) then
		if not (BlowDeath.SavedSettings[tostring(Nick)].DamageTo) then BlowDeath.SavedSettings[tostring(Nick)].DamageTo = 0 end
		if not (BlowDeath.SavedSettings[tostring(Nick)].MitigatedTo) then BlowDeath.SavedSettings[tostring(Nick)].MitigatedTo = 0 end
		if not (BlowDeath.SavedSettings[tostring(Nick)].CritTo) then BlowDeath.SavedSettings[tostring(Nick)].CritTo = 0 end
		if ( not DBSETTINGS.totalDamageTo ) or (DBSETTINGS.totalDamageTo == nil) then DBSETTINGS.totalDamageTo = 0 end
		if ( not DBSETTINGS.totalMitigatedTo ) or (DBSETTINGS.totalMitigatedTo == nil) then DBSETTINGS.totalMitigatedTo = 0 end
		if ( not DBSETTINGS.totalCritTo ) or (DBSETTINGS.totalCritTo == nil) then DBSETTINGS.totalCritTo = 0 end

--		TextLogAddEntry("Chat", 21,L"[DB] Debug: "..towstring(Nick)..L" "..towstring(Damage)..L" "..towstring(Mitigated))
		
	-- Look for Critical hits
		if msg:find(txt_critically) then 
--		TextLogAddEntry("Chat", 21,L"[DB] Debug: CRIT!! "..towstring(Nick))
		BlowDeath.SavedSettings[tostring(Nick)].CritTo = BlowDeath.SavedSettings[tostring(Nick)].CritTo+1
		DBSETTINGS.totalCritTo = DBSETTINGS.totalCritTo+1 
		end
			BlowDeath.SavedSettings[tostring(Nick)].MitigatedTo = BlowDeath.SavedSettings[tostring(Nick)].MitigatedTo+Mitigated
			BlowDeath.SavedSettings[tostring(Nick)].DamageTo = BlowDeath.SavedSettings[tostring(Nick)].DamageTo+Damage
			DBSETTINGS.totalDamageTo = DBSETTINGS.totalDamageTo+Damage
			DBSETTINGS.totalMitigatedTo = DBSETTINGS.totalMitigatedTo+Mitigated
	
--TextLogAddEntry("Chat", 21,L"[DB] Debug: "..Nick..L" "..Damage..L" "..Mitigated..L" ("..BlowDeath.SavedSettings[tostring(Nick)].DamageTo..L" / "..BlowDeath.SavedSettings[tostring(Nick)].MitigatedTo..L" * "..BlowDeath.SavedSettings[tostring(Nick)].CritTo)
--	BlowDeath.ScoreAll(tostring(Nick),L"<icon=43>",txt_Unknown,L"?",L"1")	
		end
	end
--end

end
--DEFENDENG!
if ( filterType == ChatSettings.Channels[1001].id)then

--DODGE?
	if msg:find(txt_dodged) then 
if Language == 1 then
_,Nick2,_ = msg:match( L"([%a]+) dodged ([%a]+)'s ([^%.]+)")
elseif Language == 2 then
 _,_,Nick2 = msg:match( L"([%a]+) esquivé ([^%.]+) de ([%a]+).")
elseif Language == 3 then
Nick2,_ = msg:match( L"Ihr seid ([%a]+) ([^%.]+) ausgewichen.")
elseif Language == 4 then
_,Nick2 = msg:match( L"Tu Schivato ([^%.]+) d['’i]%s?([%a]+).")
elseif Language == 5 then
Nick2 = msg:match( L"Tú esquivado [^%.]+ de ([%a]+).")	
elseif Language == 10 then
_,Nick2,_ = msg:match( L"([^%.]+) %(([%a]+)%) ([^%.]+) %(вы%) уклоняется.")	
end
	if (Nick2 ~= nil) then 
	if not BlowDeath.SavedSettings[tostring(Nick2)] then BlowDeath.ScoreAll(tostring(Nick2),L"<icon=43>",txt_Unknown,L"?",L"1")	end
--	TextLogAddEntry("Chat", 21,L"[DB] Debug: DODGE!! "..towstring(Nick2)) 
		if (BlowDeath.SavedSettings[tostring(Nick2)] ~= nil) then
		if not (BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom) or (BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom == nil) then BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom = 0 end
		if not (BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom) or (BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom == nil) then BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom = 0 end
		if ( not DBSETTINGS.totalDodgeFrom ) or (DBSETTINGS.totalDodgeFrom == nil) then DBSETTINGS.totalDodgeFrom = 0 end
		BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom = BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom+1
		DBSETTINGS.totalDodgeFrom = DBSETTINGS.totalDodgeFrom+1	
--		TextLogAddEntry("Chat", 21,L"[DB] Debug: Dodged: "..BlowDeath.SavedSettings[tostring(Nick2)].DodgeFrom) 
	end
		end
	end

	if msg:find(txt_disrupted) then 
if Language == 1 then
_,Nick2,_ = msg:match(L"([%a]+) disrupted ([%a]+)'s ([^%.]+)")
elseif Language == 2 then
_,_,Nick2 = msg:match( L"([%a]+) dissipé ([^%.]+) de ([%a]+).")
elseif Language == 3 then
Nick2,_ = msg:match( L"Ihr habt ([%a]+) ([^%.]+) gestört.")	 
elseif Language == 4 then
_,Nick2 = msg:match( L"Tu interrotto ([^%.]+) d['’i]%s?([%a]+).")
elseif Language == 5 then
Nick2 = msg:match( L"Tú trastocado [^%.]+ de ([%a]+).")	 
elseif Language == 10 then	
_,Nick2,_ = msg:match( L"([^%.]+) %(([%a]+)%) ([^%.]+) %(вы%) предотвращает срабатывание.")
	 
end
	if (Nick2 ~= nil) then 
	if not BlowDeath.SavedSettings[tostring(Nick2)] then BlowDeath.ScoreAll(tostring(Nick2),L"<icon=43>",txt_Unknown,L"?",L"1")	end
--	TextLogAddEntry("Chat", 21,L"[DB] Debug: DISRUPT!! "..towstring(Nick2)) 
		if (BlowDeath.SavedSettings[tostring(Nick2)] ~= nil) then
		if not (BlowDeath.SavedSettings[tostring(Nick2)].DisruptFrom) or (BlowDeath.SavedSettings[tostring(Nick2)].DisruptFrom == nil) then BlowDeath.SavedSettings[tostring(Nick2)].DisruptFrom = 0 end
		if ( not DBSETTINGS.totalDisruptFrom ) or (DBSETTINGS.totalDisruptFrom == nil) then DBSETTINGS.totalDisruptFrom = 0 end
		BlowDeath.SavedSettings[tostring(Nick2)].DisruptFrom = BlowDeath.SavedSettings[tostring(Nick2)].DisruptFrom+1
		DBSETTINGS.totalDisruptFrom = DBSETTINGS.totalDisruptFrom+1	
--		TextLogAddEntry("Chat", 21,L"[DB] Debug: Disrupted: "..BlowDeath.SavedSettings[tostring(Nick2)].DisruptFrom) 
		end	
		end
	end
	
		if msg:find(txt_blocked) then 
if Language == 1 then
_,Nick2,_ = msg:match( L"([%a]+) blocked ([%a]+)'s ([^%.]+)")
elseif Language == 2 then
_,_,Nick2 = msg:match( L"([%a]+) bloqué ([^%.]+) de ([%a]+).")	
elseif Language == 3 then	
Nick2,_ = msg:match( L"Ihr habt ([%a]+) ([^%.]+) geblockt.")
elseif Language == 4 then
_,Nick2 = msg:match( L"Tu Bloccato ([^%.]+) d['’i]%s?([%a]+).")
elseif Language == 5 then
Nick2 = msg:match( L"Tú bloqueado [^%.]+ de ([%a]+).")	
elseif Language == 10 then	
_,Nick2,_ = msg:match( L"([^%.]+) %(([%a]+)%) ([^%.]+) %(вы%) ставит блок.")
end
		if (Nick2 ~= nil) then 
		if not BlowDeath.SavedSettings[tostring(Nick2)] then BlowDeath.ScoreAll(tostring(Nick2),L"<icon=43>",txt_Unknown,L"?",L"1")	end
--		TextLogAddEntry("Chat", 21,L"[DB] Debug: BLOCK!! "..towstring(Nick2)) 
		if (BlowDeath.SavedSettings[tostring(Nick2)] ~= nil) then
		if not (BlowDeath.SavedSettings[tostring(Nick2)].BlockFrom) or (BlowDeath.SavedSettings[tostring(Nick2)].BlockFrom == nil) then BlowDeath.SavedSettings[tostring(Nick2)].BlockFrom = 0 end
		if ( not DBSETTINGS.totalBlockFrom ) or (DBSETTINGS.totalBlockFrom == nil) then DBSETTINGS.totalBlockFrom = 0 end
		BlowDeath.SavedSettings[tostring(Nick2)].BlockFrom = BlowDeath.SavedSettings[tostring(Nick2)].BlockFrom+1
		DBSETTINGS.totalBlockFrom = DBSETTINGS.totalBlockFrom+1	
--		TextLogAddEntry("Chat", 21,L"[DB] Debug: Blocked: "..BlowDeath.SavedSettings[tostring(Nick2)].BlockFrom) 
		end
		end
	end
	
if msg:find(txt_parried) then 
if Language == 1 then
 _,Nick2,_ = msg:match( L"([%a]+) parried ([%a]+)'s ([^%.]+)")			 
elseif Language == 2 then
 _,_,Nick2 = msg:match( L"([%a]+) paré ([^%.]+) de ([%a]+).")
 elseif Language == 3 then	
Nick2,_ = msg:match( L"Ihr habt ([%a]+) ([^%.]+) pariert.")	 
elseif Language == 4 then
_,Nick2 = msg:match( L"Tu parato ([^%.]+) d['’i]%s?([%a]+).")
elseif Language == 5 then
Nick2 = msg:match( L"Tú parar [^%.]+ de ([%a]+).")	
 elseif Language == 10 then	
_,Nick2,_ = msg:match( L"([^%.]+) %(([%a]+)%) ([^%.]+) %(вы%) парирует.")			 
end
			if (Nick2 ~= nil) then 	
		if not BlowDeath.SavedSettings[tostring(Nick2)] then BlowDeath.ScoreAll(tostring(Nick2),L"<icon=43>",txt_Unknown,L"?",L"1")	end
--		TextLogAddEntry("Chat", 21,L"[DB] Debug: PARRY!! "..towstring(Nick2)) 
		if (BlowDeath.SavedSettings[tostring(Nick2)] ~= nil) then
		if not (BlowDeath.SavedSettings[tostring(Nick2)].ParryFrom) or (BlowDeath.SavedSettings[tostring(Nick2)].ParryFrom == nil) then BlowDeath.SavedSettings[tostring(Nick2)].ParryFrom = 0 end
		if ( not DBSETTINGS.totalParryFrom ) or (DBSETTINGS.totalParryFrom == nil) then DBSETTINGS.totalParryFrom = 0 end
		BlowDeath.SavedSettings[tostring(Nick2)].ParryFrom = BlowDeath.SavedSettings[tostring(Nick2)].ParryFrom+1
		DBSETTINGS.totalParryFrom = DBSETTINGS.totalParryFrom+1	
--		TextLogAddEntry("Chat", 21,L"[DB] Debug: Parried: "..BlowDeath.SavedSettings[tostring(Nick2)].ParryFrom) 
	end
		end
	end
	
	
-- Recieving Damage and Mitigation
if Language == 1 then
Nick,_,Damage,Mitigated = msg:match( L"([%a]+)'s ([^%.]+) hits you for (%d+) damage. %((%d+) mitigated%)")
elseif Language == 2 then
_,Nick,_ = msg:match( L"([^%.]+) ([%a]+) vous ([^%.]+) vous .+")
_,Damage,Mitigated = msg:match( L"([^%.]+) subissez (%d+) points de dégâts. %((%d+) atténué%)")
elseif Language == 3 then
Nick,_,_,Damage,Mitigated = msg:match( L"([%a]+) trifft ([^%.]+) und ([^%.]+) verursacht (%d+) Schadenspunkte. %(Um (%d+) Punkte abgeschwächt%)")
elseif Language == 4 then
_,Nick,Damage,Mitigated = msg:match( L"([^%.]+) di ([%a]+) ti infligge (%d+) da[^%.]+.%((%d+) mitigati%)")
elseif Language == 5 then
Nick,Damage,Mitigated = msg:match( L"[^%.]+ de ([%a]+) te gol[^%.]+ causando (%d+) de daño. %((%d+) mitigado%)")
elseif Language == 10 then
Nick,_,Damage,_,Mitigated = msg:match( L"([%a]+) ([^%.]+) вам (%d+) ед. ([^%.]+).%(ослаблено: (%d+)%)")
end
if (Nick ~= nil) then 	
if not BlowDeath.SavedSettings[tostring(Nick)] then BlowDeath.ScoreAll(tostring(Nick),L"<icon=43>",txt_Unknown,L"?",L"1")	end
if (BlowDeath.SavedSettings[tostring(Nick)] ~= nil) then
if not (BlowDeath.SavedSettings[tostring(Nick)].DamageFrom) then BlowDeath.SavedSettings[tostring(Nick)].DamageFrom = 0 end
if not (BlowDeath.SavedSettings[tostring(Nick)].MitigatedFrom) then BlowDeath.SavedSettings[tostring(Nick)].MitigatedFrom = 0 end
if not (BlowDeath.SavedSettings[tostring(Nick)].CritFrom) then BlowDeath.SavedSettings[tostring(Nick)].CritFrom = 0 end
if ( not DBSETTINGS.totalDamageFrom ) or (DBSETTINGS.totalDamageFrom == nil) then DBSETTINGS.totalDamageFrom = 0 end
if ( not DBSETTINGS.totalMitigatedFrom ) or (DBSETTINGS.totalMitigatedFrom == nil) then DBSETTINGS.totalMitigatedFrom = 0 end
if ( not DBSETTINGS.totalCritFrom ) or (DBSETTINGS.totalCritFrom == nil) then DBSETTINGS.totalCritFrom = 0 end

	--	TextLogAddEntry("Chat", 11,L"[DB] Debug: "..towstring(Nick)..L" "..towstring(Damage)..L" "..towstring(Mitigated))

-- Look for Critical hits
if msg:find(txt_critically) then 
--TextLogAddEntry("Chat", 11,L"[DB] Debug: CRIT!! "..towstring(Nick))
BlowDeath.SavedSettings[tostring(Nick)].CritFrom = BlowDeath.SavedSettings[tostring(Nick)].CritFrom+1
DBSETTINGS.totalCritFrom = DBSETTINGS.totalCritFrom+1 
end
BlowDeath.SavedSettings[tostring(Nick)].MitigatedFrom = BlowDeath.SavedSettings[tostring(Nick)].MitigatedFrom+Mitigated
BlowDeath.SavedSettings[tostring(Nick)].DamageFrom = BlowDeath.SavedSettings[tostring(Nick)].DamageFrom+Damage
DBSETTINGS.totalDamageFrom = DBSETTINGS.totalDamageFrom+Damage
DBSETTINGS.totalMitigatedFrom = DBSETTINGS.totalMitigatedFrom+Mitigated

--TextLogAddEntry("Chat", 11,L"[DB] Debug: "..Nick..L" "..Damage..L" "..Mitigated..L" ("..BlowDeath.SavedSettings[tostring(Nick)].DamageFrom..L" / "..BlowDeath.SavedSettings[tostring(Nick)].MitigatedFrom..L" * "..BlowDeath.SavedSettings[tostring(Nick)].CritFrom..L") TOTAL: "..towstring(DBSETTINGS.totalDamageFrom)..L" / "..towstring(DBSETTINGS.totalMitigatedFrom))
end
end
end
------------------------------------------------------------------	
--================================================================	
------------------------------------------------------------------		
	if not ( filterType == SystemData.ChatLogFilters.RVR_KILLS_ORDER 
	or filterType == SystemData.ChatLogFilters.RVR_KILLS_DESTRUCTION ) then 




	return 
	end


--COMBATLOG LANGUAGES	
if(Language==1) then	
victim, verb, killer, weapon, location = msg:match(L"([%a]+) has been ([%a]+) by ([%a]+)'s ([%a%d%p ]+) in ([^%.]+).")

elseif(Language==2) then	

victim = msg:match(L"([%a]+).")
_, verb,_ = msg:match(L"([^%.]+) ([%a]+) par ([^%.]+).")
_, weapon = msg:match(L"([^%.]+) par ([^%.]+) de.")
_, killer = msg:match(L"([^%.]+) de ([%a]+).")
_, location = msg:match(L"([^%.]+) dans ([^%.]+).")

elseif (Language==3) then

victim = msg:match(L"([%a]+).")
_, killer = msg:match(L"([^%.]+) wurde von ([%a]+).")
 _, _, weapon, verb = msg:match(L"([^%.]+) mit ([%a]+) ([%a%d%p ]+) ([%a]+).")
_,location,_  = msg:match(L"([^%.]+) dem ([%a%d%p ]+) mit ([^%.]+).")

elseif(Language==4) then	
victim,verb,weapon,killer,location = msg:match(L"([%a]+) è stato ([%a]+) da ([^%.]+) di ([%a]+) a ([^%.]+).")

elseif(Language==5) then	
victim,verb,killer,weapon, location = msg:match(L"([%a]+) ha sido ([%a%d%p ]+) por ([%a]+)'s ([^%.]+) en ([^%.]+).")

elseif(Language==10) then	
victim,verb,killer,weapon, location = msg:match(L"Игрок ([%a]+) ([%a]+) игроком ([%a]+) ([%a%d%p ]+) на территории ([^%.]+).")

end

	if (killer == victim) then 
		if (sessionkillspree < killspree) then sessionkillspree = killspree end
		if (DBSETTINGS.killspree < killspree) then DBSETTINGS.killspree = killspree end
		killspree = 0		
	return 
	end --sucidies
	
	if ( killer == PlayerName ) then
		local strvictim = tostring(victim)	
		local returnkill,returndeath,returnclass,returnrank,returnclassname = Deathblow.ScoreAll(strvictim,1,timestamp,weapon,location)
		local scorecount = (returnkill-returndeath)
		if not BlowDeath.SavedSettings[tostring(victim)] then BlowDeath.ScoreAll(tostring(victim),L"<icon=43>",txt_Unknown,L"?",L"1")	end
		if DBSETTINGS.supresskill == 0 then
		SystemData.AlertText.VecType = {11,16,11,4,21}
--		SystemData.AlertText.VecText = {L""..txt_OnKill1..verb,victim,L""..txt_OnKill2,weapon,returnkill..L"/"..returndeath..L" "..txt_Score..L": "..scorecount..L" "}
		SystemData.AlertText.VecText = {L""..txt_OnKill1..verb,victim,L""..returnclass..returnclassname..L" Rank:"..returnrank,weapon,returnkill..L"/"..returndeath..L" "..txt_Score..L": "..scorecount..L" "}
		AlertTextWindow.AddAlert()
		PlaySound(215)
		tempclass = returnclass
		end

--Chatbubble with Hyperlink		
		if (DBSETTINGS.showspeach ==0) then ChatManager.AddChatText( GameData.Player.worldObjNum,L""..txt_My..CreateHyperLink(weapon,weapon, {75,95,240}, {} )..L" "..towstring(verb)..L" "..tempclass..CreateHyperLink(victim,victim, {255,0,0}, {} ))end
--		SendChatText(towstring("/say My <LINK data=\"0\" text=\"")..weapon..L"\" color=\"75,95,240\">"..L" "..verb..L" <LINK data=\"0\" text=\""..victim..L"\" color=\"255,50,50\">",L"")
--		TextLogAddEntry("Chat", 21,L"My "..weapon..L" "..verb..L" "..victim)
killspree = killspree +1
if (sessionkillspree < killspree) then sessionkillspree = killspree end
if (DBSETTINGS.killspree < killspree) then DBSETTINGS.killspree = killspree end
DBSETTINGS.lastkillname = tostring(victim)
DBSETTINGS.lastkilltime = timestamp
DBSETTINGS.lastkillweapon = weapon	
DBSETTINGS.lastkillclass = Deathblow.SavedSettings[DBSETTINGS.lastkillname].class
DBSETTINGS.supresskill = DBSETTINGS.supresskill
DBSETTINGS.supressdeath = DBSETTINGS.supressdeath
DBSETTINGS.supresstext = DBSETTINGS.supresstext
DBSETTINGS.showspeach = DBSETTINGS.showspeach
DBSETTINGS.totalDamageTo = DBSETTINGS.totalDamageTo
DBSETTINGS.totalMitigatedTo = DBSETTINGS.totalMitigatedTo
DBSETTINGS.totalCritTo = DBSETTINGS.totalCritTo
DBSETTINGS.totalDamageFrom = DBSETTINGS.totalDamageFrom
DBSETTINGS.totalMitigatedFrom = DBSETTINGS.totalMitigatedFrom
DBSETTINGS.totalCritFrom = DBSETTINGS.totalCritFrom
DBSETTINGS.totalDodgeTo = DBSETTINGS.totalDodgeTo
DBSETTINGS.totalDodgeFrom = DBSETTINGS.totalDodgeFrom
DBSETTINGS.totalDisruptTo = DBSETTINGS.totalDisruptTo
DBSETTINGS.totalDisruptFrom = DBSETTINGS.totalDisruptFrom
DBSETTINGS.totalBlockTo = DBSETTINGS.totalBlockTo
DBSETTINGS.totalBlockFrom = DBSETTINGS.totalBlockFrom
DBSETTINGS.totalParryTo = DBSETTINGS.totalParryTo
DBSETTINGS.totalParryFrom = DBSETTINGS.totalParryFrom

	end
	if ( victim == PlayerName ) then
	local strplayer = tostring(killer)
	local returnkill,returndeath,returnclass,returnrank,returnclassname = Deathblow.ScoreAll(strplayer,2,timestamp,weapon,location)
	local scorecount = (returnkill-returndeath)
		if DBSETTINGS.supressdeath == 0 then
		SystemData.AlertText.VecType = {11,16,11,4,12}
		SystemData.AlertText.VecText = {L""..txt_OnDeath1..verb..L" "..txt_OnDeath2,killer..txt_s,L""..returnclass..returnclassname..L" Rank:"..returnrank,weapon,returnkill..L"/"..returndeath..L" "..txt_Score..L": "..scorecount..L" "}
		AlertTextWindow.AddAlert()
		PlaySound(218)
		
		end
if (sessionkillspree < killspree) then sessionkillspree = killspree end
if (DBSETTINGS.killspree < killspree) then DBSETTINGS.killspree = killspree end

killspree = 0		
DBSETTINGS.lastdeathname = tostring(killer)
DBSETTINGS.lastdeathtime = timestamp
DBSETTINGS.lastdeathweapon = weapon	
DBSETTINGS.lastdeathclass = Deathblow.SavedSettings[DBSETTINGS.lastdeathname].class
DBSETTINGS.supresskill = DBSETTINGS.supresskill
DBSETTINGS.supressdeath = DBSETTINGS.supressdeath
DBSETTINGS.supresstext = DBSETTINGS.supresstext
DBSETTINGS.showspeach = DBSETTINGS.showspeach
DBSETTINGS.totalDamageTo = DBSETTINGS.totalDamageTo
DBSETTINGS.totalMitigatedTo = DBSETTINGS.totalMitigatedTo
DBSETTINGS.totalCritTo = DBSETTINGS.totalCritTo
DBSETTINGS.totalDamageFrom = DBSETTINGS.totalDamageFrom
DBSETTINGS.totalMitigatedFrom = DBSETTINGS.totalMitigatedFrom
DBSETTINGS.totalCritFrom = DBSETTINGS.totalCritFrom
DBSETTINGS.totalDodgeTo = DBSETTINGS.totalDodgeTo
DBSETTINGS.totalDodgeFrom = DBSETTINGS.totalDodgeFrom
DBSETTINGS.totalDisruptTo = DBSETTINGS.totalDisruptTo
DBSETTINGS.totalDisruptFrom = DBSETTINGS.totalDisruptFrom
DBSETTINGS.totalBlockTo = DBSETTINGS.totalBlockTo
DBSETTINGS.totalBlockFrom = DBSETTINGS.totalBlockFrom
DBSETTINGS.totalParryTo = DBSETTINGS.totalParryTo
DBSETTINGS.totalParryFrom = DBSETTINGS.totalParryFrom
end
	
if (DBSETTINGS.killspree == nill) then DBSETTINGS.killspree = 0 end
settings = DBSETTINGS
if (sessionkillspree < killspree) then sessionkillspree = killspree end
if (DBSETTINGS.killspree < killspree) then DBSETTINGS.killspree = killspree end

Deathblow.Display()	


end

function Deathblow.ScoreAll(index, temptype,timestamp,weapon,location)-- Calculating Kills and deaths, and setting up careers



	if ( not Deathblow.SavedSettings ) then
		Deathblow.SavedSettings = {}
	end

	if (temptype == 3 and not Deathblow.SavedSettings[index]) then
	TextLogAddEntry("Chat", 21,L"[DeathBlow]: No stats found for "..towstring(index))
	return
	end

	if ( not Deathblow.SavedSettings[index]) then
		Deathblow.SavedSettings[index] = { kills = 0, deaths = 0, class=L"<icon=43>", rank = L"?", classname=txt_Unknown, classID = 0,KillTime=txt_None,DeathTime=txt_None ,FirstDeathTime = txt_None ,FirstKillTime = txt_None ,KillLocation = "---",DeathLocation = "---" , KillWeapon= "---", DeathWeapon="---"}
	end

	if (BlowDeath.SavedSettings[index] ~= nil) then
	Deathblow.SavedSettings[index].class = BlowDeath.SavedSettings[index].icon
	Deathblow.SavedSettings[index].classname = towstring(BlowDeath.SavedSettings[index].classname)
	Deathblow.SavedSettings[index].rank = BlowDeath.SavedSettings[index].rank
	Deathblow.SavedSettings[index].classID = BlowDeath.SavedSettings[index].classID
	else 
	Deathblow.SavedSettings[index].class = L"<icon=43>"
	Deathblow.SavedSettings[index].classname  = txt_Unknown
	Deathblow.SavedSettings[index].rank = L"?"
	Deathblow.SavedSettings[index].classID = 0
	end

if (temptype ~= 3) then
	if ( not DBsession.list ) then
	DBsession.list ={}
	end

	if ( not DBsession.list[index]) then	
	DBsession.list[index] = { kills = 0, deaths = 0, class=L"<icon=43>", rank = L"?", classname=txt_Unknown, classID=0, KillTime=txt_None,DeathTime=txt_None ,FirstDeathTime = txt_None ,FirstKillTime = txt_None ,KillLocation = "---",DeathLocation = "---" , KillWeapon= "---", DeathWeapon="---"}
	end
	
	if (DBsession.list[index] ~= nil) and (BlowDeath.SavedSettings[index] ~= nil)then
	DBsession.list[index].class = BlowDeath.SavedSettings[index].icon
	DBsession.list[index].classname = BlowDeath.SavedSettings[index].classname
	DBsession.list[index].rank = BlowDeath.SavedSettings[index].rank
	DBsession.list[index].classID = BlowDeath.SavedSettings[index].classID
	else 
	DBsession.list[index].class = L"<icon=43>"
	DBsession.list[index].classname = txt_Unknown
	DBsession.list[index].rank = L"?"
	DBsession.list[index].classID = 0	
	end
end
local textcolor = 0
	
	if temptype == 1 then
		if (Deathblow.SavedSettings[index].kills == nil) or (Deathblow.SavedSettings[index].kills == 0) then Deathblow.SavedSettings[index].FirstKillTime = timestamp end
		Deathblow.SavedSettings[index].kills = Deathblow.SavedSettings[index].kills+1
		Deathblow.SavedSettings[index].KillTime = timestamp
		Deathblow.SavedSettings[index].KillWeapon = weapon
		Deathblow.SavedSettings[index].KillLocation = location
		DBsession.list[index].kills = DBsession.list[index].kills+1
		textcolor = 21


		elseif temptype == 2 then
		if (Deathblow.SavedSettings[index].deaths == nil) or (Deathblow.SavedSettings[index].deaths == 0) then Deathblow.SavedSettings[index].FirstDeathTime = timestamp end	
		Deathblow.SavedSettings[index].deaths = Deathblow.SavedSettings[index].deaths+1
		Deathblow.SavedSettings[index].DeathTime = timestamp
		Deathblow.SavedSettings[index].DeathWeapon = weapon
		Deathblow.SavedSettings[index].DeathLocation = location
		DBsession.list[index].deaths = DBsession.list[index].deaths+1
		textcolor = 11
		end

	local	currentscore = (Deathblow.SavedSettings[index].kills - Deathblow.SavedSettings[index].deaths)
	if DBSETTINGS.supresstext == 0 then
	TextLogAddEntry("Chat", textcolor,L"[DeathBlow]: "..Deathblow.SavedSettings[index].class..L" <LINK data=\"Profile:"..towstring(index)..L"\" text=\""..towstring(index)..L"\" color=\"255,218,73\"> ("..towstring(Deathblow.SavedSettings[index].rank)..L"): "..Deathblow.SavedSettings[index].kills..L"/"..Deathblow.SavedSettings[index].deaths..L". "..txt_Score..L":"..currentscore)
	end
	Deathblow.Settings = Deathblow.SavedSettings[index]
	Settings = Deathblow.Settings	
	local returndeath =Deathblow.SavedSettings[index].deaths
	local returnkill =Deathblow.SavedSettings[index].kills
	local returnclass =Deathblow.SavedSettings[index].class	
	local returnclassname =Deathblow.SavedSettings[index].classname
	local returnrank =Deathblow.SavedSettings[index].rank	
	local returnclassID =Deathblow.SavedSettings[index].classID	
		
	return returnkill,returndeath,returnclass,returnrank,returnclassname,returnclassID
end

function trim(s)
	return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end



function Deathblow.Display() -- indexing players and stats into a table



if ListMode == 0 then
if ( not Deathblow.SavedSettings) then return end


totalkills = 0
totaldeaths = 0
totalscore = 0
totalratio = 0
_DisplayOrder = {}
local shtcount = 1
local ratiocalc = 0
  DB.Listdata = {}
	for k, v in pairs(Deathblow.SavedSettings) do
--	TextLogAddEntry("Chat", 21,towstring(k)..L" "..towstring(Deathblow.SavedSettings[k].kills)..L"/"..towstring(Deathblow.SavedSettings[k].deaths)..L" Score: "..towstring(Deathblow.SavedSettings[k].kills - Deathblow.SavedSettings[k].deaths))
	if (Deathblow.SavedSettings[k].kills == 0) and (Deathblow.SavedSettings[k].deaths >= 0) then ratiocalc = (0 - Deathblow.SavedSettings[k].deaths)
	elseif (Deathblow.SavedSettings[k].kills >= 0) and (Deathblow.SavedSettings[k].deaths == 0) then ratiocalc = Deathblow.SavedSettings[k].kills
	else  ratiocalc = (Deathblow.SavedSettings[k].kills/Deathblow.SavedSettings[k].deaths) end

	table.insert(_DisplayOrder, shtcount)
	table.insert(DB.Listdata, { Title = towstring(k), kills = Deathblow.SavedSettings[k].kills, deaths = Deathblow.SavedSettings[k].deaths, score = tostring(Deathblow.SavedSettings[k].kills) - tostring(Deathblow.SavedSettings[k].deaths), icon =Deathblow.SavedSettings[k].class, rank =Deathblow.SavedSettings[k].rank, ratio = wstring.format(L"%.01f",ratiocalc)})
	shtcount = shtcount+1
	totalkills=(totalkills+Deathblow.SavedSettings[k].kills)
	totaldeaths=(totaldeaths+Deathblow.SavedSettings[k].deaths)
	end
	sortlist(DB.Listdata,_DisplayOrder)

	totalscore = (totalkills-totaldeaths)


	if (totalkills == 0) and (totaldeaths >= 0) then totalratio = (0 - totaldeaths)
	elseif (totalkills >= 0) and (totaldeaths == 0) then totalratio = totalkills
	else  totalratio = (totalkills/totaldeaths) end
	


	ListBoxSetDisplayOrder(DB.winList, _DisplayOrder)
--	ListBoxSetDisplayOrder(DeathblowWindowList, _DisplayOrder)
	LabelSetText(Deathblow.StatValues0, towstring(PlayerName)..txt_s..txt_Total)
	LabelSetText(Deathblow.StatValues1, towstring(totalkills))
	LabelSetText(Deathblow.StatValues2, towstring(totaldeaths))
	LabelSetText(Deathblow.StatValues3, GetNumSymbol(totalscore))
	LabelSetText(Deathblow.StatValues4, towstring(shtcount-1))
--	LabelSetText(Deathblow.StatValues5, towstring(shtcount-1))
	LabelSetText(Deathblow.StatValues6, wstring.format(L"%.01f",totalratio))

WindowSetShowing("DeathblowWin1WindowList", true)
WindowSetShowing("SummaryWindow", false)
WindowSetShowing("ProfileWindow", false)	
	
elseif ListMode == 1 then


totalkills = 0
totaldeaths = 0
totalscore = 0
_DisplayOrder = {}
local shtcount = 1
local ratiocalc = 0
  DB.Listdata = {}

 if (DBsession.list) then
  for k, v in pairs(DBsession.list) do
--	TextLogAddEntry("Chat", 21,towstring(k)..L" "..towstring(Deathblow.SavedSettings[k].kills)..L"/"..towstring(Deathblow.SavedSettings[k].deaths)..L" Score: "..towstring(Deathblow.SavedSettings[k].kills - Deathblow.SavedSettings[k].deaths))
	if (DBsession.list[k].kills == 0) and (DBsession.list[k].deaths >= 0) then ratiocalc = (0 - DBsession.list[k].deaths)
	elseif (DBsession.list[k].kills >= 0) and (DBsession.list[k].deaths == 0) then ratiocalc = DBsession.list[k].kills
	else  ratiocalc = (DBsession.list[k].kills/DBsession.list[k].deaths) end


	table.insert(_DisplayOrder, shtcount)
	table.insert(DB.Listdata, { Title = towstring(k), kills = DBsession.list[k].kills, deaths = DBsession.list[k].deaths, score = tostring(DBsession.list[k].kills) - tostring(DBsession.list[k].deaths), icon =DBsession.list[k].class, rank=DBsession.list[k].rank, ratio = wstring.format(L"%.01f",ratiocalc)})
	shtcount = shtcount+1
	totalkills=(totalkills+Deathblow.SavedSettings[k].kills)
	totaldeaths=(totaldeaths+Deathblow.SavedSettings[k].deaths)
	end
end
	sortlist(DB.Listdata,_DisplayOrder)
	totalscore = (totalkills-totaldeaths)

	if (totalkills == 0) and (totaldeaths >= 0) then totalratio = (0 - totaldeaths)
	elseif (totalkills >= 0) and (totaldeaths == 0) then totalratio = totalkills
	else  totalratio = (totalkills/totaldeaths) end
	
	ListBoxSetDisplayOrder(DB.winList, _DisplayOrder)
--	ListBoxSetDisplayOrder(DeathblowWindowList, _DisplayOrder)
	LabelSetText(Deathblow.StatValues0, towstring(PlayerName)..txt_s..txt_Session..L":")
	LabelSetText(Deathblow.StatValues1, towstring(totalkills))
	LabelSetText(Deathblow.StatValues2, towstring(totaldeaths))
	LabelSetText(Deathblow.StatValues3, GetNumSymbol(totalscore))
	LabelSetText(Deathblow.StatValues4, towstring(shtcount-1))
--	LabelSetText(Deathblow.StatValues5,L"0")
	LabelSetText(Deathblow.StatValues6, wstring.format(L"%.01f",totalratio))
	
	
WindowSetShowing("DeathblowWin1WindowList", true)
WindowSetShowing("SummaryWindow", false)
WindowSetShowing("ProfileWindow", false)	
	
elseif ListMode == 2 then


Testcalling()


----------------------------------------------------------------------------------------------	
	
elseif ListMode == 3 then

----------------------------------------------------------------------------------------------
Testcalling()

sessionkills = 0
sessiondeaths = 0
sessionscore = 0

local sessioncount = 0
local sessionratio = 0


 if (DBsession.list) then
  for k, v in pairs(DBsession.list) do
--	TextLogAddEntry("Chat", 21,towstring(k)..L" "..towstring(Deathblow.SavedSettings[k].kills)..L"/"..towstring(Deathblow.SavedSettings[k].deaths)..L" Score: "..towstring(Deathblow.SavedSettings[k].kills - Deathblow.SavedSettings[k].deaths))
	if (DBsession.list[k].kills == 0) and (DBsession.list[k].deaths >= 0) then sessionratio = (0 - DBsession.list[k].deaths)
	elseif (DBsession.list[k].kills >= 0) and (DBsession.list[k].deaths == 0) then sessionratio = DBsession.list[k].kills
	else  sessionratio = (DBsession.list[k].kills/DBsession.list[k].deaths) end

	sessioncount = sessioncount+1
	sessionkills=(sessionkills+DBsession.list[k].kills)
	sessiondeaths=(sessiondeaths+DBsession.list[k].deaths)
	end
end
	
	sessionscore = (sessionkills-sessiondeaths)

	if (sessionkills == 0) and (sessiondeaths >= 0) then sessionratio = (0 - sessiondeaths)
	elseif (sessionkills >= 0) and (sessiondeaths == 0) then sessionratio = sessionkills
	else  sessionratio = (sessionkills/sessiondeaths) end


totalkills = 0
totaldeaths = 0
totalscore = 0
totalratio = 0

local shtcount = 0
local ratiocalc = 0

	for k, v in pairs(Deathblow.SavedSettings) do
--	TextLogAddEntry("Chat", 21,towstring(k)..L" "..towstring(Deathblow.SavedSettings[k].kills)..L"/"..towstring(Deathblow.SavedSettings[k].deaths)..L" Score: "..towstring(Deathblow.SavedSettings[k].kills - Deathblow.SavedSettings[k].deaths))
	if (Deathblow.SavedSettings[k].kills == 0) and (Deathblow.SavedSettings[k].deaths >= 0) then ratiocalc = (0 - Deathblow.SavedSettings[k].deaths)
	elseif (Deathblow.SavedSettings[k].kills >= 0) and (Deathblow.SavedSettings[k].deaths == 0) then ratiocalc = Deathblow.SavedSettings[k].kills
	else  ratiocalc = (Deathblow.SavedSettings[k].kills/Deathblow.SavedSettings[k].deaths) end

	shtcount = shtcount+1
	totalkills=(totalkills+Deathblow.SavedSettings[k].kills)
	totaldeaths=(totaldeaths+Deathblow.SavedSettings[k].deaths)
	end


	if (totalkills == 0) and (totaldeaths >= 0) then totalratio = (0 - totaldeaths)
	elseif (totalkills >= 0) and (totaldeaths == 0) then totalratio = totalkills
	else  totalratio = (totalkills/totaldeaths) end
	




if ( not DBSETTINGS.lastkillname ) then DBSETTINGS.lastkillname = "" end
	if ( not DBSETTINGS.lastkilltime ) then DBSETTINGS.lastkilltime = "" end
if ( not DBSETTINGS.lastkillweapon ) then DBSETTINGS.lastkillweapon = "" end
if ( not DBSETTINGS.lastkillclass ) then DBSETTINGS.lastkillclass = "" end

if ( not DBSETTINGS.lastdeathname ) then DBSETTINGS.lastdeathname = "" end
	if ( not DBSETTINGS.lastdeathtime ) then DBSETTINGS.lastdeathtime = "" end
if ( not DBSETTINGS.lastdeathweapon ) then DBSETTINGS.lastdeathweapon = "" end	
if ( not DBSETTINGS.lastdeathclass ) then DBSETTINGS.lastdeathclass = "" end		
	
	
WindowSetShowing("DeathblowWin1WindowList", false)
WindowSetShowing("SummaryWindow", true)
WindowSetShowing("ProfileWindow", false)

LabelSetText(DBSummary.ListTitle1, towstring(careerlist[1].icon)..L" "..towstring(careerlist[1].className))
LabelSetText(DBSummary.ListTitle2, towstring(careerlist[2].icon)..L" "..towstring(careerlist[2].className))
LabelSetText(DBSummary.ListTitle3, towstring(careerlist[3].icon)..L" "..towstring(careerlist[3].className))
LabelSetText(DBSummary.ListTitle4, towstring(careerlist[4].icon)..L" "..towstring(careerlist[4].className))
LabelSetText(DBSummary.ListTitle5, towstring(careerlist[5].icon)..L" "..towstring(careerlist[5].className))
LabelSetText(DBSummary.ListTitle6, towstring(careerlist[6].icon)..L" "..towstring(careerlist[6].className))
LabelSetText(DBSummary.ListTitle7, towstring(careerlist[7].icon)..L" "..towstring(careerlist[7].className))
LabelSetText(DBSummary.ListTitle8, towstring(careerlist[8].icon)..L" "..towstring(careerlist[8].className))
LabelSetText(DBSummary.ListTitle9, towstring(careerlist[9].icon)..L" "..towstring(careerlist[9].className))
LabelSetText(DBSummary.ListTitle10, towstring(careerlist[10].icon)..L" "..towstring(careerlist[10].className))
LabelSetText(DBSummary.ListTitle11, towstring(careerlist[11].icon)..L" "..towstring(careerlist[11].className))
LabelSetText(DBSummary.ListTitle12, towstring(careerlist[12].icon)..L" "..towstring(careerlist[12].className))

LabelSetText(DBSummary.ListValue1, L"("..towstring(careerlist[1].kills)..L"/"..towstring(careerlist[1].deaths)..L")")
LabelSetText(DBSummary.ListValue2, L"("..towstring(careerlist[2].kills)..L"/"..towstring(careerlist[2].deaths)..L")")
LabelSetText(DBSummary.ListValue3, L"("..towstring(careerlist[3].kills)..L"/"..towstring(careerlist[3].deaths)..L")")
LabelSetText(DBSummary.ListValue4, L"("..towstring(careerlist[4].kills)..L"/"..towstring(careerlist[4].deaths)..L")")
LabelSetText(DBSummary.ListValue5, L"("..towstring(careerlist[5].kills)..L"/"..towstring(careerlist[5].deaths)..L")")
LabelSetText(DBSummary.ListValue6, L"("..towstring(careerlist[6].kills)..L"/"..towstring(careerlist[6].deaths)..L")")
LabelSetText(DBSummary.ListValue7, L"("..towstring(careerlist[7].kills)..L"/"..towstring(careerlist[7].deaths)..L")")
LabelSetText(DBSummary.ListValue8, L"("..towstring(careerlist[8].kills)..L"/"..towstring(careerlist[8].deaths)..L")")
LabelSetText(DBSummary.ListValue9, L"("..towstring(careerlist[9].kills)..L"/"..towstring(careerlist[9].deaths)..L")")
LabelSetText(DBSummary.ListValue10, L"("..towstring(careerlist[10].kills)..L"/"..towstring(careerlist[10].deaths)..L")")
LabelSetText(DBSummary.ListValue11, L"("..towstring(careerlist[11].kills)..L"/"..towstring(careerlist[11].deaths)..L")")
LabelSetText(DBSummary.ListValue12, L"("..towstring(careerlist[12].kills)..L"/"..towstring(careerlist[12].deaths)..L")")


	LabelSetText(DBSummary.TitleValues0, towstring(PlayerName)..txt_s..txt_Overall_Stats)
	LabelSetText(DBSummary.TitleValues1, txt_Kills)
	LabelSetText(DBSummary.TitleValues2, txt_Deaths)
	LabelSetText(DBSummary.TitleValues3, txt_Score)
	LabelSetText(DBSummary.TitleValues4, txt_Enemys)
	LabelSetText(DBSummary.TitleValues5, txt_Ratio)
	LabelSetText(DBSummary.TitleValues6, txt_Kills)
	LabelSetText(DBSummary.TitleValues7, txt_Deaths)
	LabelSetText(DBSummary.TitleValues8, txt_Score)
	LabelSetText(DBSummary.TitleValues9, txt_Enemys)
	LabelSetText(DBSummary.TitleValues10, txt_Ratio)		
	LabelSetText(DBSummary.TitleValues11, txt_Total)	
	LabelSetText(DBSummary.TitleValues12, txt_Session)	
	
	LabelSetText(DBSummary.TitleValues13, txt_Latest_Kill)
	LabelSetText(DBSummary.TitleValues14, txt_Latest_Death)
	LabelSetText(DBSummary.TitleValues15, txt_Class_Stats)

	LabelSetText(DBSummary.killspreeTitle, txt_Killspree)
	LabelSetText(DBSummary.killspreeValue, towstring(DBSETTINGS.killspree))

	

	
		LabelSetText(DBsession.killspreeTitle, txt_Killspree)
	LabelSetText(DBsession.killspreeValue, towstring(sessionkillspree))
	
	
	LabelSetText(DBSummary.StatValues1, towstring(totalkills))
	LabelSetText(DBSummary.StatValues2, towstring(totaldeaths))
	
	if (tonumber(totalkills-totaldeaths) > 0) then LabelSetTextColor(DBSummary.StatValues3, 0, 255, 0)
	elseif (tonumber(totalkills-totaldeaths) < 0) then LabelSetTextColor(DBSummary.StatValues3, 255, 0, 0)
	else LabelSetTextColor(DBSummary.StatValues3, 255, 218, 73) end
	
	LabelSetText(DBSummary.StatValues3, GetNumSymbol(totalkills-totaldeaths))
	LabelSetText(DBSummary.StatValues4, towstring(shtcount))
	LabelSetText(DBSummary.StatValues5, wstring.format(L"%.01f",totalratio))
	
	
	
	LabelSetText(DBSummary.StatValues6, towstring(sessionkills))
	LabelSetText(DBSummary.StatValues7, towstring(sessiondeaths))
	if (tonumber(sessionkills-sessiondeaths) > 0) then LabelSetTextColor(DBSummary.StatValues8, 0, 255, 0)
	elseif (tonumber(sessionkills-sessiondeaths) < 0) then LabelSetTextColor(DBSummary.StatValues8, 255, 0, 0)
	else LabelSetTextColor(DBSummary.StatValues8, 255, 218, 73) end
	LabelSetText(DBSummary.StatValues8, GetNumSymbol(sessionkills-sessiondeaths))
	LabelSetText(DBSummary.StatValues9, towstring(sessioncount))
	LabelSetText(DBSummary.StatValues10, wstring.format(L"%.01f",sessionratio))
	

	
if BlowDeath.SavedSettings[DBSETTINGS.lastkillname] then DBSETTINGS.lastkillclass = BlowDeath.SavedSettings[DBSETTINGS.lastkillname].icon end
if BlowDeath.SavedSettings[DBSETTINGS.lastdeathname] then DBSETTINGS.lastdeathclass = BlowDeath.SavedSettings[DBSETTINGS.lastdeathname].icon end
	
local link  = CreateHyperLink(L"Slask",L"slask", {255,28,160}, {} )	

ButtonSetText("DBSummarystatLastKillButton",towstring(DBSETTINGS.lastkillclass)..L" "..towstring(DBSETTINGS.lastkillname)..L"  ("..towstring(DBSETTINGS.lastkilltime)..L")")
ButtonSetText("DBSummarystatLastDeathButton",towstring(DBSETTINGS.lastdeathclass)..L" "..towstring(DBSETTINGS.lastdeathname)..L"  ("..towstring(DBSETTINGS.lastdeathtime)..L")")

LabelSetText(DBSummary.StatValues0,towstring(version))

LabelSetText("SummaryTotalDamageTitle",txt_Total_Damage)
LabelSetText("SummaryTotalDamageSeperator",L"/")

LabelSetText("SummaryTotalDamageTo",towstring(math.floor(DBSETTINGS.totalDamageTo/1000))..L"K")


LabelSetText("SummaryTotalDamageFrom",towstring(math.floor(DBSETTINGS.totalDamageFrom/1000))..L"K")

LabelSetText("SummaryTotalMitigateTitle",txt_Total_Mitigated)
LabelSetText("SummaryTotalMitigateSeperator",L"/")
LabelSetText("SummaryTotalMitigateTo",towstring(math.floor(DBSETTINGS.totalMitigatedTo/1000))..L"K")
LabelSetText("SummaryTotalMitigateFrom",towstring(math.floor(DBSETTINGS.totalMitigatedFrom/1000))..L"K")

LabelSetText("SummaryTotalCritTitle",txt_Total_Crits)
LabelSetText("SummaryTotalCritSeperator",L"<icon=43>")
LabelSetText("SummaryTotalCritTo",towstring(DBSETTINGS.totalCritTo))
LabelSetText("SummaryTotalCritFrom",towstring(DBSETTINGS.totalCritFrom))

LabelSetText("SummaryTotalDodgeTitle",txt_Total_Dodges)
LabelSetText("SummaryTotalDodgeTo",towstring(DBSETTINGS.totalDodgeTo))
LabelSetText("SummaryTotalDodgeFrom",towstring(DBSETTINGS.totalDodgeFrom))

LabelSetText("SummaryTotalBlockTitle",txt_Total_Blocks)
LabelSetText("SummaryTotalBlockTo",towstring(DBSETTINGS.totalBlockTo))
LabelSetText("SummaryTotalBlockFrom",towstring(DBSETTINGS.totalBlockFrom))

LabelSetText("SummaryTotalDisruptTitle",txt_Total_Disrupts)
LabelSetText("SummaryTotalDisruptTo",towstring(DBSETTINGS.totalDisruptTo))
LabelSetText("SummaryTotalDisruptFrom",towstring(DBSETTINGS.totalDisruptFrom))

LabelSetText("SummaryTotalParryTitle",txt_Total_Parries)
LabelSetText("SummaryTotalParryTo",towstring(DBSETTINGS.totalParryTo))
LabelSetText("SummaryTotalParryFrom",towstring(DBSETTINGS.totalParryFrom))



----------------------------------------------------------------------------------------------	
	
elseif ListMode == 4 then -- Player Profiles



ProfileName = tostring(ProfileName)
local ProfileSessionRatio = 0
local ProfileSessionKill= 0
local ProfileSessionDeath = 0
local ProfileSessionScore = 0

if (DBsession.list) then
  for k, v in pairs(DBsession.list) do
--	TextLogAddEntry("Chat", 21,towstring(k)..L" "..towstring(Deathblow.SavedSettings[k].kills)..L"/"..towstring(Deathblow.SavedSettings[k].deaths)..L" Score: "..towstring(Deathblow.SavedSettings[k].kills - Deathblow.SavedSettings[k].deaths))
--	if (DBsession.list[ProfileName].kills == 0) and (DBsession.list[ProfileName].deaths >= 0) then ProfileSessionRatio = (0 - DBsession.list[ProfileName].deaths)
--	elseif (DBsession.list[ProfileName].kills >= 0) and (DBsession.list[ProfileName].deaths == 0) then ProfileSessionRatio = DBsession.list[ProfileName].kills
--	else  ProfileSessionRatio = (DBsession.list[ProfileName].kills/DBsession.list[ProfileName].deaths) end


if (DBsession.list[ProfileName] ~= nil) then ProfileSessionKill=(DBsession.list[ProfileName].kills) else ProfileSessionKill = 0 break end
if (DBsession.list[ProfileName] ~= nil) then ProfileSessionDeath=(DBsession.list[ProfileName].deaths) else ProfileSessionDeath = 0 break end
 	ProfileSessionScore=( ProfileSessionKill- ProfileSessionDeath)
	end

end
	

	if (ProfileSessionKill == 0) and (ProfileSessionDeath >= 0) then ProfileSessionRatio = (0 - ProfileSessionDeath)
	elseif (ProfileSessionKill >= 0) and (ProfileSessionDeath == 0) then ProfileSessionRatio = ProfileSessionKill
	else  ProfileSessionRatio = (ProfileSessionKill/ProfileSessionDeath) end	
	
--ProfileSessionRatio = (ProfileSessionKill-ProfileSessionDeath)

local ProfileKill = Deathblow.SavedSettings[ProfileName].kills
local ProfileDeath = Deathblow.SavedSettings[ProfileName].deaths
local ProfileScore = GetNumSymbol(Deathblow.SavedSettings[ProfileName].kills-Deathblow.SavedSettings[ProfileName].deaths)
local ProfileRatio = 0


	if (ProfileKill == 0) and (ProfileDeath >= 0) then ProfileRatio = (0 - ProfileDeath)
	elseif (ProfileKill >= 0) and (ProfileDeath == 0) then ProfileRatio = ProfileKill
	else  ProfileRatio = (ProfileKill/ProfileDeath) end


if BlowDeath.SavedSettings[tostring(ProfileName)] ~= nil then	

if not (BlowDeath.SavedSettings[tostring(ProfileName)].DamageTo) then BlowDeath.SavedSettings[tostring(ProfileName)].DamageTo = 0 end
if not (BlowDeath.SavedSettings[tostring(ProfileName)].MitigatedTo) then BlowDeath.SavedSettings[tostring(ProfileName)].MitigatedTo = 0 end
if not (BlowDeath.SavedSettings[tostring(ProfileName)].CritTo) then BlowDeath.SavedSettings[tostring(ProfileName)].CritTo = 0 end
if not (BlowDeath.SavedSettings[tostring(ProfileName)].DamageFrom) then BlowDeath.SavedSettings[tostring(ProfileName)].DamageFrom = 0 end
if not (BlowDeath.SavedSettings[tostring(ProfileName)].MitigatedFrom) then BlowDeath.SavedSettings[tostring(ProfileName)].MitigatedFrom = 0 end
if not (BlowDeath.SavedSettings[tostring(ProfileName)].CritFrom) then BlowDeath.SavedSettings[tostring(ProfileName)].CritFrom = 0 end	
if not (BlowDeath.SavedSettings[tostring(ProfileName)].DodgeTo) then BlowDeath.SavedSettings[tostring(ProfileName)].DodgeTo = 0 end	
if not (BlowDeath.SavedSettings[tostring(ProfileName)].DodgeFrom) then BlowDeath.SavedSettings[tostring(ProfileName)].DodgeFrom = 0 end	
if not (BlowDeath.SavedSettings[tostring(ProfileName)].DisruptTo) then BlowDeath.SavedSettings[tostring(ProfileName)].DisruptTo = 0 end	
if not (BlowDeath.SavedSettings[tostring(ProfileName)].DisruptFrom) then BlowDeath.SavedSettings[tostring(ProfileName)].DisruptFrom = 0 end	
if not (BlowDeath.SavedSettings[tostring(ProfileName)].BlockTo) then BlowDeath.SavedSettings[tostring(ProfileName)].BlockTo = 0 end	
if not (BlowDeath.SavedSettings[tostring(ProfileName)].BlockFrom) then BlowDeath.SavedSettings[tostring(ProfileName)].BlockFrom = 0 end	
if not (BlowDeath.SavedSettings[tostring(ProfileName)].ParryTo) then BlowDeath.SavedSettings[tostring(ProfileName)].ParryTo = 0 end	
if not (BlowDeath.SavedSettings[tostring(ProfileName)].ParryFrom) then BlowDeath.SavedSettings[tostring(ProfileName)].ParryFrom = 0 end	
end	
	
WindowSetShowing("DeathblowWin1WindowList", false)
WindowSetShowing("SummaryWindow", false)
WindowSetShowing("ProfileWindow", true)

LabelSetText(DBProfile.TitleValues0, towstring(ProfileName))
LabelSetText(DBProfile.Title1, towstring(Deathblow.SavedSettings[ProfileName].class)..L" "..towstring(Deathblow.SavedSettings[ProfileName].classname)..L" - "..txt_Level..towstring(Deathblow.SavedSettings[ProfileName].rank)..L"    ")


LabelSetText(DBProfile.Title2, txt_Encounters)
LabelSetText(DBProfile.Title3, txt_Kills)
LabelSetText(DBProfile.Title4, txt_Deaths)
LabelSetText(DBProfile.Title5, txt_Score)
LabelSetText(DBProfile.Title6, txt_Ratio)

LabelSetText(DBProfile.Title7, txt_Encounters)
LabelSetText(DBProfile.Title8, txt_Kills)
LabelSetText(DBProfile.Title9, txt_Deaths)
LabelSetText(DBProfile.Title10, txt_Score)
LabelSetText(DBProfile.Title11, txt_Ratio)
LabelSetText(DBProfile.Title12, txt_First_Kill)
LabelSetText(DBProfile.Title13, txt_First_Death)
LabelSetText(DBProfile.Title14, txt_Total)
LabelSetText(DBProfile.Title15, txt_Session)

LabelSetText(DBProfile.Title16, txt_Latest_Kill)
LabelSetText(DBProfile.Title17, txt_Latest_Death)

LabelSetText(DBProfile.Title19, txt_Damage)
LabelSetText(DBProfile.Title20, txt_Mitigated)
LabelSetText(DBProfile.Title21, txt_Criticalls)

LabelSetText(DBProfile.Title22, L"/")
LabelSetText(DBProfile.Title23, L"/")
LabelSetText(DBProfile.Title24, L"<icon=43>")

LabelSetText(DBProfile.Title25, txt_Dodged)
LabelSetText(DBProfile.Title26, txt_Blocked)
LabelSetText(DBProfile.Title27, txt_Disrupted)
LabelSetText(DBProfile.Title28, txt_Parried)

--LabelSetText(DBProfile.Title29, L"Dodged")

LabelSetText(DBProfile.Value2, towstring(ProfileKill+ProfileDeath))
LabelSetText(DBProfile.Value3, towstring(ProfileKill))
LabelSetText(DBProfile.Value4, towstring(ProfileDeath))

if (tonumber(ProfileScore) > 0) then LabelSetTextColor(DBProfile.Value5, 0, 255, 0)
elseif (tonumber(ProfileScore) < 0) then LabelSetTextColor(DBProfile.Value5, 255, 0, 0)
else LabelSetTextColor(DBProfile.Value5, 255, 218, 73) end
LabelSetText(DBProfile.Value5, towstring(ProfileScore))

LabelSetText(DBProfile.Value6, wstring.format(L"%.01f",ProfileRatio))
LabelSetText(DBProfile.Value7, towstring(ProfileSessionKill+ProfileSessionDeath))
LabelSetText(DBProfile.Value8, towstring(ProfileSessionKill))
LabelSetText(DBProfile.Value9, towstring(ProfileSessionDeath))

if (tonumber(ProfileSessionScore) > 0) then LabelSetTextColor(DBProfile.Value10, 0, 255, 0)
elseif (tonumber(ProfileSessionScore) < 0) then LabelSetTextColor(DBProfile.Value10, 255, 0, 0)
else LabelSetTextColor(DBProfile.Value10, 255, 218, 73) end
LabelSetText(DBProfile.Value10, GetNumSymbol(ProfileSessionScore))



LabelSetText(DBProfile.Value11, wstring.format(L"%.01f",ProfileSessionRatio))
LabelSetText(DBProfile.Value12, towstring(Deathblow.SavedSettings[ProfileName].FirstKillTime))
LabelSetText(DBProfile.Value13, towstring(Deathblow.SavedSettings[ProfileName].FirstDeathTime))

LabelSetText(DBProfile.Value16, towstring(Deathblow.SavedSettings[ProfileName].KillTime)..L"<br>"..towstring(Deathblow.SavedSettings[ProfileName].KillLocation)..L"<br>"..towstring(Deathblow.SavedSettings[ProfileName].KillWeapon))
LabelSetText(DBProfile.Value17, towstring(Deathblow.SavedSettings[ProfileName].DeathTime)..L"<br>"..towstring(Deathblow.SavedSettings[ProfileName].DeathLocation)..L"<br>"..towstring(Deathblow.SavedSettings[ProfileName].DeathWeapon))
LabelSetText(DBProfile.Value18, towstring(Deathblow.SavedSettings[ProfileName].class))

if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value19, towstring(BlowDeath.SavedSettings[ProfileName].DamageTo)) else LabelSetText(DBProfile.Value19, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value20, towstring(BlowDeath.SavedSettings[ProfileName].MitigatedFrom)) else LabelSetText(DBProfile.Value20, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value21, towstring(BlowDeath.SavedSettings[ProfileName].CritTo)) else LabelSetText(DBProfile.Value21, L"-") end

if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value22, towstring(BlowDeath.SavedSettings[ProfileName].DamageFrom)) else LabelSetText(DBProfile.Value22, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value23, towstring(BlowDeath.SavedSettings[ProfileName].MitigatedTo)) else LabelSetText(DBProfile.Value23, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value24, towstring(BlowDeath.SavedSettings[ProfileName].CritFrom)) else LabelSetText(DBProfile.Value24, L"-") end

if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value25, towstring(BlowDeath.SavedSettings[ProfileName].DodgeTo)) else LabelSetText(DBProfile.Value25, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value26, towstring(BlowDeath.SavedSettings[ProfileName].BlockTo)) else LabelSetText(DBProfile.Value26, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value27, towstring(BlowDeath.SavedSettings[ProfileName].DisruptTo)) else LabelSetText(DBProfile.Value27, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value28, towstring(BlowDeath.SavedSettings[ProfileName].ParryTo)) else LabelSetText(DBProfile.Value28, L"-") end

if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value29, towstring(BlowDeath.SavedSettings[ProfileName].DodgeFrom)) else LabelSetText(DBProfile.Value29, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value30, towstring(BlowDeath.SavedSettings[ProfileName].BlockFrom)) else LabelSetText(DBProfile.Value30, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value31, towstring(BlowDeath.SavedSettings[ProfileName].DisruptFrom)) else LabelSetText(DBProfile.Value31, L"-") end
if BlowDeath.SavedSettings[tostring(ProfileName)] then LabelSetText(DBProfile.Value32, towstring(BlowDeath.SavedSettings[ProfileName].ParryFrom)) else LabelSetText(DBProfile.Value32, L"-") end




--LabelSetText(DBProfile.Title1, L"Slesk")


local careerPictures = {44,46,45,42,92,93,63,64,51,49,48,50,14,16,17,15,78,77,76,75,33,31,32,34}

--if ( not TomeIsBestiarySpeciesUnlocked( tostring(Deathblow.SavedSettings[ProfileName].classID) ) ) then for i = 1, 150 do if ( TomeIsBestiarySpeciesUnlocked( i ) ) then careerPicID = i break end end end
if BlowDeath.SavedSettings[tostring(ProfileName)] and (Deathblow.SavedSettings[ProfileName].classID) and (TomeIsBestiarySpeciesUnlocked(careerPictures[tonumber(Deathblow.SavedSettings[ProfileName].classID)])) then
TomeSetBestiarySpeciesImage(careerPictures[tonumber(Deathblow.SavedSettings[ProfileName].classID)])
else
TomeSetBestiarySpeciesImage(0)
end

----------------------------------------------------------------------------------------------

		
--	DBSETTINGS = { lastdeathname = killer, lastdeathtime = timestamp, lastdeathweapon = weapon}	
end--list mode end		

if (DBsession.list == nil) then ButtonSetDisabledFlag( "DeathblowWin1WindowSessionTab",true) else  ButtonSetDisabledFlag( "DeathblowWin1WindowSessionTab",false) end
if (ProfileName == nil) then ButtonSetDisabledFlag( "DeathblowWin1WindowProfileTab",true) else ButtonSetDisabledFlag( "DeathblowWin1WindowProfileTab",false) end
if (Deathblow.SavedSettings == nil) then ButtonSetDisabledFlag( "DeathblowWin1WindowSummTab",true); ButtonSetDisabledFlag( "DeathblowWin1WindowTotalTab",true); ButtonSetDisabledFlag( "DeathblowWin1WindowClassTab",true) else 
ButtonSetDisabledFlag( "DeathblowWin1WindowSummTab",false); ButtonSetDisabledFlag( "DeathblowWin1WindowTotalTab",false); ButtonSetDisabledFlag( "DeathblowWin1WindowClassTab",false)
end




if (not DBSETTINGS) then DBSETTINGS = { lastdeathname = "", lastdeathtime = "", lastdeathweapon = "", lastdeathclass = "", lastkillname = "", lastkilltime = "", lastkillweapon = "", lastkillclass = "",killspree=0,supresskill = 0,supressdeath=0,supresstext=0,showspeach=0,totalDamageTo=0,totalMitigatedTo=0,totalCritTo=0,totalDodgeTo=0,totalBlockTo=0,totalDisruptTo=0,totalParryTo=0,totalDamageFrom=0,totalMitigatedFrom=0,totalCritFrom=0,totalDodgeFrom=0,totalDisruptFrom=0,totalBlockFrom=0,totalParryFrom=0} end	
if ( not DBSETTINGS.killspree ) or (DBSETTINGS.killspree == nill) then DBSETTINGS.killspree = 0 end
if ( not DBSETTINGS.supresskill ) or (DBSETTINGS.supresskill == nil) then DBSETTINGS.supresskill = 0 end
if ( not DBSETTINGS.supressdeath ) or (DBSETTINGS.supressdeath == nil) then DBSETTINGS.supressdeath = 0 end
if ( not DBSETTINGS.supresstext ) or (DBSETTINGS.supresstext == nil) then DBSETTINGS.supresstext = 0 end
if ( not DBSETTINGS.showspeach ) or (DBSETTINGS.showspeach == nil) then DBSETTINGS.showspeach = 0 end
if ( not DBSETTINGS.totalDamageTo ) or (DBSETTINGS.totalDamageTo == nil) then DBSETTINGS.totalDamageTo = 0 end
if ( not DBSETTINGS.totalMitigatedTo ) or (DBSETTINGS.totalMitigatedTo == nil) then DBSETTINGS.totalMitigatedTo = 0 end
if ( not DBSETTINGS.totalCritTo ) or (DBSETTINGS.totalCritTo == nil) then DBSETTINGS.totalCritTo = 0 end
if ( not DBSETTINGS.totalDamageFrom ) or (DBSETTINGS.totalDamageFrom == nil) then DBSETTINGS.totalDamageFrom = 0 end
if ( not DBSETTINGS.totalMitigatedFrom ) or (DBSETTINGS.totalMitigatedFrom == nil) then DBSETTINGS.totalMitigatedFrom = 0 end
if ( not DBSETTINGS.totalCritFrom ) or (DBSETTINGS.totalCritFrom == nil) then DBSETTINGS.totalCritFrom = 0 end
if ( not DBSETTINGS.totalDodgeTo ) or (DBSETTINGS.totalDodgeTo == nil) then DBSETTINGS.totalDodgeTo = 0 end
if ( not DBSETTINGS.totalDodgeFrom ) or (DBSETTINGS.totalDodgeFrom == nil) then DBSETTINGS.totalDodgeFrom = 0 end
if ( not DBSETTINGS.totalBlockTo ) or (DBSETTINGS.totalBlockTo == nil) then DBSETTINGS.totalBlockTo = 0 end
if ( not DBSETTINGS.totalBlockFrom ) or (DBSETTINGS.totalBlockFrom == nil) then DBSETTINGS.totalBlockFrom = 0 end
if ( not DBSETTINGS.totalDisruptTo ) or (DBSETTINGS.totalDisruptTo == nil) then DBSETTINGS.totalDisruptTo = 0 end
if ( not DBSETTINGS.totalDisruptFrom ) or (DBSETTINGS.totalDisruptFrom == nil) then DBSETTINGS.totalDisruptFrom = 0 end
if ( not DBSETTINGS.totalParryTo ) or (DBSETTINGS.totalParryTo == nil) then DBSETTINGS.totalParryTo = 0 end
if ( not DBSETTINGS.totalParryFrom ) or (DBSETTINGS.totalParryFrom == nil) then DBSETTINGS.totalParryFrom = 0 end

end--function end
	
		
function Testcalling ()

totalkills = 0
totaldeaths = 0
totalscore = 0
_DisplayOrder = {}
local shtcount = 1
  DB.Listdata = {}

careerlist = {}

for i=0, 12 do
careerlist[i] = { className="",icon=L"<icon=43>",kills = 0, deaths = 0, number=0,battles=0, score=0}
end


for k, v in pairs(Deathblow.SavedSettings) do


	if Deathblow.SavedSettings[k].class == L"<icon=43>" then careerlist[0].icon = towstring(Deathblow.SavedSettings[k].class);    careerlist[0].number = careerlist[0].number+1; careerlist[0].kills = careerlist[0].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[0].deaths = careerlist[0].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[0].score = (towstring(careerlist[0].kills) - towstring(careerlist[0].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20189>" or Deathblow.SavedSettings[k].class == L"<icon=20182>" then careerlist[1].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[1].number = careerlist[1].number+1; careerlist[1].kills = careerlist[1].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[1].deaths = careerlist[1].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[1].score = (towstring(careerlist[1].kills) - towstring(careerlist[1].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20188>" or Deathblow.SavedSettings[k].class == L"<icon=20184>" then careerlist[2].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[2].number = careerlist[2].number+1;careerlist[2].kills = careerlist[2].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[2].deaths = careerlist[2].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[2].score = (towstring(careerlist[2].kills) - towstring(careerlist[2].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20193>" or Deathblow.SavedSettings[k].class == L"<icon=20195>" then careerlist[3].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[3].number = careerlist[3].number+1;careerlist[3].kills = careerlist[3].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[3].deaths = careerlist[3].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[3].score = (towstring(careerlist[3].kills) - towstring(careerlist[3].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20187>" or Deathblow.SavedSettings[k].class == L"<icon=20197>" then careerlist[4].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[4].number = careerlist[4].number+1;careerlist[4].kills = careerlist[4].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[4].deaths = careerlist[4].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[4].score = (towstring(careerlist[4].kills) - towstring(careerlist[4].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20202>" or Deathblow.SavedSettings[k].class == L"<icon=20185>" then careerlist[5].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[5].number = careerlist[5].number+1;careerlist[5].kills = careerlist[5].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[5].deaths = careerlist[5].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[5].score = (towstring(careerlist[5].kills) - towstring(careerlist[5].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20190>" or Deathblow.SavedSettings[k].class == L"<icon=20192>" then careerlist[6].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[6].number = careerlist[6].number+1;careerlist[6].kills = careerlist[6].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[6].deaths = careerlist[6].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[6].score = (towstring(careerlist[6].kills) - towstring(careerlist[6].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20183>" or Deathblow.SavedSettings[k].class == L"<icon=20203>" then careerlist[7].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[7].number = careerlist[7].number+1;careerlist[7].kills = careerlist[7].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[7].deaths = careerlist[7].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[7].score = (towstring(careerlist[7].kills) - towstring(careerlist[7].deaths))	
	elseif Deathblow.SavedSettings[k].class == L"<icon=20199>" or Deathblow.SavedSettings[k].class == L"<icon=20191>" then careerlist[8].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[8].number = careerlist[8].number+1;careerlist[8].kills = careerlist[8].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[8].deaths = careerlist[8].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[8].score = (towstring(careerlist[8].kills) - towstring(careerlist[7].deaths))	
	elseif Deathblow.SavedSettings[k].class == L"<icon=20198>" or Deathblow.SavedSettings[k].class == L"<icon=20181>" then careerlist[9].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[9].number = careerlist[9].number+1;careerlist[9].kills = careerlist[9].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[9].deaths = careerlist[9].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[9].score = (towstring(careerlist[9].kills) - towstring(careerlist[9].deaths))	
	elseif Deathblow.SavedSettings[k].class == L"<icon=20194>" or Deathblow.SavedSettings[k].class == L"<icon=20201>" then careerlist[10].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[10].number = careerlist[10].number+1; careerlist[10].kills = careerlist[10].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[10].deaths = careerlist[10].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[10].score = (towstring(careerlist[10].kills) - towstring(careerlist[10].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20200>" or Deathblow.SavedSettings[k].class == L"<icon=20186>" then careerlist[11].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[11].number = careerlist[11].number+1; careerlist[11].kills = careerlist[11].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[11].deaths = careerlist[11].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[11].score = (towstring(careerlist[11].kills) - towstring(careerlist[11].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20180>" or Deathblow.SavedSettings[k].class == L"<icon=20196>" then careerlist[12].icon = towstring(Deathblow.SavedSettings[k].class); careerlist[12].number = careerlist[12].number+1; careerlist[12].kills = careerlist[12].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[12].deaths = careerlist[12].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[12].score = (towstring(careerlist[12].kills) - towstring(careerlist[12].deaths))
end
end	

if (GameData.Player.realm == 2) then  -- Class sorting for Order kills
careerlist[0].className = txt_Unknown; careerlist[0].icon = L"<icon=43>";
careerlist[1].className = towstring(GetStringFromTable("CareerNamesMale", 20)); careerlist[1].icon = L"<icon=20189>";
careerlist[2].className = towstring(GetStringFromTable("CareerNamesMale", 21)); careerlist[2].icon = L"<icon=20188>";
careerlist[3].className = towstring(GetStringFromTable("CareerNamesMale", 22)); careerlist[3].icon = L"<icon=20193>";
careerlist[4].className = towstring(GetStringFromTable("CareerNamesMale", 23)); careerlist[4].icon = L"<icon=20187>";
careerlist[5].className = towstring(GetStringFromTable("CareerNamesMale", 60)); careerlist[5].icon = L"<icon=20202>";
careerlist[6].className = txt_KotBS; careerlist[6].icon = L"<icon=20190>";
careerlist[7].className = towstring(GetStringFromTable("CareerNamesMale", 62)); careerlist[7].icon = L"<icon=20183>";
careerlist[8].className = towstring(GetStringFromTable("CareerNamesMale", 63)); careerlist[8].icon = L"<icon=20199>";
careerlist[9].className = towstring(GetStringFromTable("CareerNamesMale", 100)); careerlist[9].icon = L"<icon=20198>";
careerlist[10].className = towstring(GetStringFromTable("CareerNamesMale", 101)); careerlist[10].icon = L"<icon=20194>";
careerlist[11].className = towstring(GetStringFromTable("CareerNamesMale", 102)); careerlist[11].icon = L"<icon=20200>";
careerlist[12].className = towstring(GetStringFromTable("CareerNamesMale", 103)); careerlist[12].icon = L"<icon=20180>";
elseif (GameData.Player.realm == 1) then
careerlist[0].className = txt_Unknown; careerlist[0].icon = L"<icon=43>";
careerlist[1].className = towstring(GetStringFromTable("CareerNamesMale", 24)); careerlist[1].icon = L"<icon=20182>";
careerlist[2].className = towstring(GetStringFromTable("CareerNamesMale", 25)); careerlist[2].icon = L"<icon=20184>";
careerlist[3].className = towstring(GetStringFromTable("CareerNamesMale", 26)); careerlist[3].icon = L"<icon=20195>";
careerlist[4].className = towstring(GetStringFromTable("CareerNamesMale", 27)); careerlist[4].icon = L"<icon=20197>";
careerlist[5].className = towstring(GetStringFromTable("CareerNamesMale", 64)); careerlist[5].icon = L"<icon=20185>";
careerlist[6].className = towstring(GetStringFromTable("CareerNamesMale", 65)); careerlist[6].icon = L"<icon=20192>";
careerlist[7].className = towstring(GetStringFromTable("CareerNamesMale", 66)); careerlist[7].icon = L"<icon=20203>";
careerlist[8].className = towstring(GetStringFromTable("CareerNamesMale", 67)); careerlist[8].icon = L"<icon=20191>";
careerlist[9].className = towstring(GetStringFromTable("CareerNamesMale", 104)); careerlist[9].icon = L"<icon=20181>";
careerlist[10].className = towstring(GetStringFromTable("CareerNamesMale", 105)); careerlist[10].icon = L"<icon=20201>";
careerlist[11].className = towstring(GetStringFromTable("CareerNamesMale", 106)); careerlist[11].icon = L"<icon=20186>";
careerlist[12].className = towstring(GetStringFromTable("CareerNamesMale", 107)); careerlist[12].icon = L"<icon=20196>";

end
local ratiocalc = 0

--
	for i=0, 12 do

	if (careerlist[i].kills == 0) and (careerlist[i].deaths >= 0) then ratiocalc = (0 - careerlist[i].deaths)
	elseif (careerlist[i].kills >= 0) and (careerlist[i].deaths == 0) then ratiocalc = careerlist[i].kills
	else  ratiocalc = (careerlist[i].kills/careerlist[i].deaths) end	
		
	--	TextLogAddEntry("Chat", 21,towstring(k)..L" "..towstring(Deathblow.SavedSettings[k].kills)..L"/"..towstring(Deathblow.SavedSettings[k].deaths)..L" Score: "..towstring(Deathblow.SavedSettings[k].kills - Deathblow.SavedSettings[k].deaths))
	table.insert(_DisplayOrder, shtcount)
	table.insert(DB.Listdata, { Title = towstring(careerlist[i].className), kills = careerlist[i].kills, deaths = careerlist[i].deaths, score = tostring(careerlist[i].kills) - tostring(careerlist[i].deaths), icon =towstring(careerlist[i].icon), rank =towstring(careerlist[i].number),ratio = wstring.format(L"%.1f",ratiocalc)})
	shtcount = shtcount+1

	end
	sortlist(DB.Listdata,_DisplayOrder)
	ListBoxSetDisplayOrder(DB.winList, _DisplayOrder)
--	ListBoxSetDisplayOrder(DeathblowWindowList, _DisplayOrder)
	LabelSetText(Deathblow.StatValues0, towstring(PlayerName)..txt_s..txt_Class_Stats)
	LabelSetText(Deathblow.StatValues1, L"")
	LabelSetText(Deathblow.StatValues2, L"")
	LabelSetText(Deathblow.StatValues3, L"")
	LabelSetText(Deathblow.StatValues4, L"")
--	LabelSetText(Deathblow.StatValues5, L"")
	LabelSetText(Deathblow.StatValues6, L"")

	WindowSetShowing("DeathblowWin1WindowList", true)
	WindowSetShowing("SummaryWindow", false)
	WindowSetShowing("ProfileWindow", false)
	
end	
	
	
function DBsession.Display() -- indexing players and stats into a table

sessiontotalkills = 0
sessiontotaldeaths = 0
sessiontotalscore = 0
sessiontotalratio = 0
session_DisplayOrder = {}
local shtcount = 0
local ratiocalc = 0
 DBsession.Listdata = {}
TextLogAddEntry("Chat", 21,L"-----[DeathBlow "..txt_Session..L"]-----")
if ( not DBsession.list) then
TextLogAddEntry("Chat", 21,L"-----------------------------")
	TextLogAddEntry("Chat", 21,L"["..txt_Session..L"] "..txt_Enemys..L": "..(shtcount)..L" "..txt_Kills..L": "..sessiontotalkills..L" "..txt_Deaths..L": "..sessiontotaldeaths..L" "..txt_Score..L": "..sessiontotalscore..L" "..txt_Ratio..L": "..sessiontotalratio..L"\n")
	return 
end

	for k, v in pairs(DBsession.list) do

	if (DBsession.list[k].kills == 0) and (DBsession.list[k].deaths >= 0) then ratiocalc = (0 - DBsession.list[k].deaths)
	elseif (DBsession.list[k].kills >= 0) and (DBsession.list[k].deaths == 0) then ratiocalc = DBsession.list[k].kills
	else  ratiocalc = (DBsession.list[k].kills/DBsession.list[k].deaths) end
	
		
	TextLogAddEntry("Chat", 21,DBsession.list[k].class..L" <LINK data=\"Profile:"..towstring(k)..L"\" text=\""..towstring(k)..L"\" color=\"255,218,73\"> ("..DBsession.list[k].rank..L"): "..towstring(DBsession.list[k].kills)..L"/"..towstring(DBsession.list[k].deaths)..L" "..txt_Score..L":"..towstring(DBsession.list[k].kills - DBsession.list[k].deaths)..L" "..txt_Ratio..L": "..wstring.format(L"%.01f",ratiocalc))
	table.insert(session_DisplayOrder, shtcount)
	table.insert(DBsession.Listdata, { Title = towstring(k), kills = DBsession.list[k].kills, deaths = DBsession.list[k].deaths, score = tostring(DBsession.list[k].kills) - tostring(DBsession.list[k].deaths), icon =DBsession.list[k].class, rank =DBsession.list[k].rank})
	shtcount = shtcount+1
	sessiontotalkills=(sessiontotalkills+DBsession.list[k].kills)
	sessiontotaldeaths=(sessiontotaldeaths+DBsession.list[k].deaths)
	end
sortlist(DBsession.Listdata,session_DisplayOrder)
sessiontotalscore = (sessiontotalkills-sessiontotaldeaths)

	if (sessiontotalkills == 0) and (sessiontotaldeaths >= 0) then sessiontotalratio = (0 - sessiontotaldeaths)
	elseif (sessiontotalkills >= 0) and (sessiontotaldeaths == 0) then sessiontotalratio = sessiontotalkills
	else  sessiontotalratio = (sessiontotalkills/sessiontotaldeaths) end

TextLogAddEntry("Chat", 21,L"-----------------------------")
TextLogAddEntry("Chat", 21,L"["..txt_Session..L"] "..txt_Enemys..L": "..(shtcount)..L" "..txt_Kills..L": "..sessiontotalkills..L" "..txt_Deaths..L": "..sessiontotaldeaths..L" "..txt_Score..L": "..sessiontotalscore..L" "..txt_Ratio..L": "..wstring.format(L"%.01f",sessiontotalratio)..L"\n")
	
 
	ListBoxSetDisplayOrder(DB.winList, session_DisplayOrder)
--	ListBoxSetDisplayOrder(DeathblowWindowList, _DisplayOrder)
--	LabelSetText(Deathblow.StatValues0, towstring(PlayerName)..L"'s Session:")
--	LabelSetText(Deathblow.StatValues1, towstring(sessiontotalkills))
--	LabelSetText(Deathblow.StatValues2, towstring(sessiontotaldeaths))
--	LabelSetText(Deathblow.StatValues3, towstring(sessiontotalscore))
--	LabelSetText(Deathblow.StatValues4, towstring(shtcount-1))
end


function DB_Show()
if (WindowGetShowing(DB.winRoot) == false) then
WindowSetShowing(DB.winRoot, true)
else
WindowSetShowing(DB.winRoot, false)
WindowSetShowing("SettingsWindow", false)
end
Deathblow.Display()
end

function DB.OnLButtonUp()-- Handling the buttons in the Deathblow window (most for sorting)
	if SystemData.MouseOverWindow.name == DB.winRoot .. "CloseButton" then
		WindowSetShowing(DB.winRoot, false)
		WindowSetShowing("SettingsWindow", false)
		return
	end
--sort name
	if SystemData.MouseOverWindow.name == DB.winRoot .. "NameButton" then
		if Sorting == 0 then 
		Sorting = 1
		else Sorting = 0 end
	end	
	--sort kills
	if SystemData.MouseOverWindow.name == DB.winRoot .. "KillsButton" then
		if Sorting == 3 then 
		Sorting = 2
		else Sorting = 3	end
	end	
	--sort Deaths
	if SystemData.MouseOverWindow.name == DB.winRoot .. "DeathsButton" then
		if Sorting == 5 then 
		Sorting = 4
		else Sorting = 5 end
	end	
	--sort score
	if SystemData.MouseOverWindow.name == DB.winRoot .. "ScoreButton" then
		if Sorting == 7 then 
		Sorting = 6
		else Sorting = 7 end
	end	
	--sort icons
	if SystemData.MouseOverWindow.name == DB.winRoot .. "IconButton" then
		if Sorting == 9 then 
		Sorting = 8
		else Sorting = 9 end
	end		

		if SystemData.MouseOverWindow.name == DB.winRoot .. "RatioButton" then
		if Sorting == 11 then 
		Sorting = 10
		else Sorting = 11 end
	end		
	
	

	
Deathblow.Display()	




end


function BlowDeath.GetClass( targetClassification, targetId, targetType )--Grabbing targeted players name and their icons and converts them
	TargetInfo:UpdateFromClient()
	
	local tarName = TargetInfo:UnitName(targetClassification)
	local tarLevel = TargetInfo:UnitLevel(targetClassification)
	local tarCareerID = TargetInfo:UnitCareer(targetClassification)
	local tarCareerName = TargetInfo:UnitCareerName(targetClassification)
	local tarFriendly = TargetInfo:UnitIsFriendly(targetClassification)

	TargetNisse = tonumber(targetType)
	tarName = wstring.sub(tarName,1,-3 )
	tarCareerName = wstring.sub(tarCareerName,1,-3 )
	
	if tarFriendly then return end	
	if targetType == 5 then	


local caricon2 = towstring(L"<icon=43>")
if tarCareerID == 1 then
caricon2 = towstring(L"<icon=20189>") --IB
elseif tarCareerID == 2 then
caricon2 = towstring(L"<icon=20188>") --SL
elseif tarCareerID == 3 then
caricon2 = towstring(L"<icon=20193>") --RP
elseif tarCareerID == 4 then
caricon2 = towstring(L"<icon=20187>") --EN
elseif tarCareerID == 5 then
caricon2 = towstring(L"<icon=20182>")--BO
elseif tarCareerID == 6 then
caricon2 = towstring(L"<icon=20184>")--CH
elseif tarCareerID == 7 then
caricon2 = towstring(L"<icon=20195>")--SA
elseif tarCareerID == 8 then
caricon2 = towstring(L"<icon=20197>")--SH
elseif tarCareerID == 9 then
caricon2 = towstring(L"<icon=20202>") --WH
elseif tarCareerID == 10 then
caricon2 = towstring(L"<icon=20190>") --KN
elseif tarCareerID == 11 then
caricon2 = towstring(L"<icon=20183>") --BW
elseif tarCareerID == 12 then
caricon2 = towstring(L"<icon=20199>") --WP
elseif tarCareerID == 13 then
caricon2 = towstring(L"<icon=20185>")--CO
elseif tarCareerID == 14 then
caricon2 = towstring(L"<icon=20192>")--MA
elseif tarCareerID == 15 then
caricon2 = towstring(L"<icon=20203>")--ZE
elseif tarCareerID == 16 then
caricon2 = towstring(L"<icon=20191>")--MA
elseif tarCareerID == 17 then
caricon2 = towstring(L"<icon=20198>") --SM
elseif tarCareerID == 18 then
caricon2 = towstring(L"<icon=20194>") --SW
elseif tarCareerID == 19 then
caricon2 = towstring(L"<icon=20200>") --WL
elseif tarCareerID == 20 then
caricon2 = towstring(L"<icon=20180>") --AM
elseif tarCareerID == 21 then
caricon2 = towstring(L"<icon=20181>")--BG
elseif tarCareerID == 22 then
caricon2 = towstring(L"<icon=20201>")--WE
elseif tarCareerID == 23 then
caricon2 = towstring(L"<icon=20186>")--DK
elseif tarCareerID == 24 then
caricon2 = towstring(L"<icon=20196>")--SC
	
		end

		table.insert(tmpTargetData,{tarName, tarCareerID, tarCareerName,tarLevel})
		BlowDeath.ScoreAll(tostring(tarName),towstring(caricon2),towstring(tarCareerName),towstring(tarLevel),towstring(tarCareerID))
--		d(L"Deathblow: "..towstring(targetClassification)..L" "..towstring(tarName)..L" "..caricon2);
	end	
end


function BlowDeath.ScoreAll(index2, temptype2,temptype3,temptype4,temptype5)-- Saving trageted players's names and icons into BlowDeath.SavedSettings[playername]
if (tostring(index2) == nil) or (tostring(index2) == L"nil") then return end
	if ( not BlowDeath.SavedSettings ) then
		BlowDeath.SavedSettings = {}
	end

	if ( not BlowDeath.SavedSettings[index2]) then
		BlowDeath.SavedSettings[index2] = { icon = temptype2, classname = temptype3, rank = temptype4,classID=temptype5}
	
	end
	BlowDeath.SavedSettings[index2].icon = temptype2
	BlowDeath.SavedSettings[index2].classname = temptype3
	BlowDeath.SavedSettings[index2].rank = temptype4
	BlowDeath.SavedSettings[index2].classID = temptype5
--	TextLogAddEntry("Chat", 0,L"saving: "..towstring(index2)..L": "..BlowDeath.SavedSettings[index2].icon)
	BlowDeath.Settings = tostring(BlowDeath.SavedSettings[index2])
	Settings = BlowDeath.Settings	

	if (Deathblow.SavedSettings[index2]) then 
	Deathblow.SavedSettings[index2].class = temptype2 
	Deathblow.SavedSettings[index2].classname = temptype3
	Deathblow.SavedSettings[index2].rank = temptype4
	Deathblow.SavedSettings[index2].classID = temptype5
	
		if DBsession.list and DBsession.list[index2] and DBsession.list[index2].class then 
		DBsession.list[index2].class = Deathblow.SavedSettings[index2].class 
		end
		if DBsession.list and DBsession.list[index2] and DBsession.list[index2].classname then 
		DBsession.list[index2].classname = Deathblow.SavedSettings[index2].classname 
		end
		if DBsession.list and DBsession.list[index2] and DBsession.list[index2].rank then 
		DBsession.list[index2].rank = Deathblow.SavedSettings[index2].rank 
		end
		if DBsession.list and DBsession.list[index2] and DBsession.list[index2].classID then 
		DBsession.list[index2].classID = Deathblow.SavedSettings[index2].classID 
		end
	end
return
end

function BlowDeath.list() --List all players in gathered Icon database, might be long if you use it
TextLogAddEntry("Chat", 21,L"-----[DeathBlow list]-----")
local listcount = 0

if (not BlowDeath.SavedSettings) then --Cutoff if empty
TextLogAddEntry("Chat", 21,L"---------------------------")
TextLogAddEntry("Chat", 21,L"[List] Enemys in database:"..towstring(listcount)..L"\n")
	return
end
	for k, v in pairs(BlowDeath.SavedSettings) do
	if (BlowDeath.SavedSettings[k].rank == nil) then BlowDeath.SavedSettings[k].rank = "?" end

	listcount = listcount+1
	TextLogAddEntry("Chat", 21,towstring(k)..L" ("..towstring(BlowDeath.SavedSettings[k].rank)..L") "..towstring(BlowDeath.SavedSettings[k].icon))
	end
	TextLogAddEntry("Chat", 21,L"---------------------------")
	TextLogAddEntry("Chat", 21,L"[List] Enemys in database:"..towstring(listcount)..L"\n")
end


function BlowDeath.total() --Total listing
local listcount = 0 
local killstotal = 0
local deathstotal = 0
local scoretotal = 0
local ratiototal = 0
local ratiocalc = 0

TextLogAddEntry("Chat", 21,L"-----[Deathblow "..txt_Total..L"]-----") --deathblow total--

if (not Deathblow.SavedSettings) then
TextLogAddEntry("Chat", 21,L"---------------------------")
TextLogAddEntry("Chat", 21,L"["..txt_Total..L"] "..txt_Enemys..L":"..towstring(listcount)..L" "..txt_Kills..L":"..towstring(killstotal)..L" "..txt_Deaths..L":"..towstring(deathstotal)..L" "..txt_Score..L":"..towstring(scoretotal)..L" "..txt_Ratio..L": "..towstring(ratiototal)..L"\n")
return
end
	for k, v in pairs(Deathblow.SavedSettings) do
	
	if (Deathblow.SavedSettings[k].rank == nil) then Deathblow.SavedSettings[k].rank = L"?" end
--Ratio calculation.. no divide by 0!
	if (Deathblow.SavedSettings[k].kills == 0) and (Deathblow.SavedSettings[k].deaths >= 0) then ratiocalc = (0 - Deathblow.SavedSettings[k].deaths)
	elseif (Deathblow.SavedSettings[k].kills >= 0) and (Deathblow.SavedSettings[k].deaths == 0) then ratiocalc = Deathblow.SavedSettings[k].kills
	else  ratiocalc = (Deathblow.SavedSettings[k].kills/Deathblow.SavedSettings[k].deaths) end
	TextLogAddEntry("Chat", 21,Deathblow.SavedSettings[k].class..L"("..towstring(Deathblow.SavedSettings[k].rank)..L") <LINK data=\"Profile:"..towstring(k)..L"\" text=\""..towstring(k)..L"\" color=\"255,218,73\"> "..towstring(Deathblow.SavedSettings[k].kills)..L"/"..towstring(Deathblow.SavedSettings[k].deaths)..L" "..txt_Score..L": "..towstring(Deathblow.SavedSettings[k].kills - Deathblow.SavedSettings[k].deaths)..L" "..txt_Ratio..L": "..wstring.format(L"%.01f",ratiocalc))
	listcount = listcount+1
	killstotal = (killstotal + Deathblow.SavedSettings[k].kills)
	deathstotal = (deathstotal + Deathblow.SavedSettings[k].deaths)
	end
	scoretotal = (killstotal-deathstotal)

	if (killstotal == 0) and (deathstotal >= 0) then ratiototal = (0 - deathstotal)
	elseif (killstotal >= 0) and (deathstotal == 0) then ratiototal = killstotal
	else  ratiototal = (killstotal/deathstotal) end
	
	TextLogAddEntry("Chat", 21,L"---------------------------")
	TextLogAddEntry("Chat", 21,L"["..txt_Total..L"] "..txt_Enemys..L":"..towstring(listcount)..L" "..txt_Kills..L":"..towstring(killstotal)..L" "..txt_Deaths..L":"..towstring(deathstotal)..L" "..txt_Score..L":"..towstring(scoretotal)..L" "..txt_Ratio..L": "..wstring.format(L"%.01f",ratiototal)..L"\n")
end





function BlowDeath.career()--Career listing
careerlist = {}
for i=0, 13 do
careerlist[i] = { kills = 0, deaths = 0, number=0,battles=0, score=0, ratio=0}
end
TextLogAddEntry("Chat", 21,L"-----[DeathBlow "..txt_Careers..L"]-----")

if (not Deathblow.SavedSettings) then
TextLogAddEntry("Chat", 21,L"---------------------------\n")
return

end
	for k, v in pairs(Deathblow.SavedSettings) do
	

	if Deathblow.SavedSettings[k].class == L"<icon=43>" then    careerlist[0].number = careerlist[0].number+1; careerlist[0].kills = careerlist[0].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[0].deaths = careerlist[0].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[0].score = (towstring(careerlist[0].kills) - towstring(careerlist[0].deaths));careerlist[0].ratio = towstring(((careerlist[0].kills+1) / towstring(careerlist[0].deaths+1))-1)
	elseif Deathblow.SavedSettings[k].class == L"<icon=20189>" or Deathblow.SavedSettings[k].class == L"<icon=20182>" then careerlist[1].number = careerlist[1].number+1; careerlist[1].kills = careerlist[1].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[1].deaths = careerlist[1].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[1].score = (towstring(careerlist[1].kills) - towstring(careerlist[1].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20188>" or Deathblow.SavedSettings[k].class == L"<icon=20184>" then careerlist[2].number = careerlist[2].number+1;careerlist[2].kills = careerlist[2].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[2].deaths = careerlist[2].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[2].score = (towstring(careerlist[2].kills) - towstring(careerlist[2].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20193>" or Deathblow.SavedSettings[k].class == L"<icon=20195>" then careerlist[3].number = careerlist[3].number+1;careerlist[3].kills = careerlist[3].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[3].deaths = careerlist[3].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[3].score = (towstring(careerlist[3].kills) - towstring(careerlist[3].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20187>" or Deathblow.SavedSettings[k].class == L"<icon=20197>" then careerlist[4].number = careerlist[4].number+1;careerlist[4].kills = careerlist[4].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[4].deaths = careerlist[4].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[4].score = (towstring(careerlist[4].kills) - towstring(careerlist[4].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20202>" or Deathblow.SavedSettings[k].class == L"<icon=20185>" then careerlist[5].number = careerlist[5].number+1;careerlist[5].kills = careerlist[5].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[5].deaths = careerlist[5].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[5].score = (towstring(careerlist[5].kills) - towstring(careerlist[5].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20202>" or Deathblow.SavedSettings[k].class == L"<icon=20192>" then careerlist[6].number = careerlist[6].number+1;careerlist[6].kills = careerlist[6].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[6].deaths = careerlist[6].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[6].score = (towstring(careerlist[6].kills) - towstring(careerlist[6].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20183>" or Deathblow.SavedSettings[k].class == L"<icon=20203>" then careerlist[7].number = careerlist[7].number+1;careerlist[7].kills = careerlist[7].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[7].deaths = careerlist[7].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[7].score = (towstring(careerlist[7].kills) - towstring(careerlist[7].deaths))	
	elseif Deathblow.SavedSettings[k].class == L"<icon=20199>" or Deathblow.SavedSettings[k].class == L"<icon=20191>" then careerlist[8].number = careerlist[8].number+1;careerlist[8].kills = careerlist[8].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[8].deaths = careerlist[8].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[8].score = (towstring(careerlist[8].kills) - towstring(careerlist[7].deaths))	
	elseif Deathblow.SavedSettings[k].class == L"<icon=20198>" or Deathblow.SavedSettings[k].class == L"<icon=20181>" then careerlist[9].number = careerlist[9].number+1;careerlist[9].kills = careerlist[9].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[9].deaths = careerlist[9].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[9].score = (towstring(careerlist[9].kills) - towstring(careerlist[9].deaths))	
	elseif Deathblow.SavedSettings[k].class == L"<icon=20194>" or Deathblow.SavedSettings[k].class == L"<icon=20201>" then careerlist[10].number = careerlist[10].number+1; careerlist[10].kills = careerlist[10].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[10].deaths = careerlist[10].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[10].score = (towstring(careerlist[10].kills) - towstring(careerlist[10].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20200>" or Deathblow.SavedSettings[k].class == L"<icon=20186>" then careerlist[11].number = careerlist[11].number+1; careerlist[11].kills = careerlist[11].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[11].deaths = careerlist[11].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[11].score = (towstring(careerlist[11].kills) - towstring(careerlist[11].deaths))
	elseif Deathblow.SavedSettings[k].class == L"<icon=20180>" or Deathblow.SavedSettings[k].class == L"<icon=20196>" then careerlist[12].number = careerlist[12].number+1; careerlist[12].kills = careerlist[12].kills+towstring(Deathblow.SavedSettings[k].kills); careerlist[12].deaths = careerlist[12].deaths+towstring(Deathblow.SavedSettings[k].deaths);careerlist[12].score = (towstring(careerlist[12].kills) - towstring(careerlist[12].deaths))
end


	end
if (GameData.Player.realm == 2) then  -- Class sorting for Order kills
	TextLogAddEntry("Chat", 21,careerlist[0].number..L" - "..towstring(careerlist[0].deaths+careerlist[0].kills)..L" <icon=43>Unknown: "..towstring(careerlist[0].kills)..L"/"..towstring(careerlist[0].deaths)..L" Score: "..towstring(careerlist[0].score)..L" Ratio: "..wstring.format(L"%.01f",careerlist[0].ratio))
	TextLogAddEntry("Chat", 21,careerlist[1].number..L" - "..towstring(careerlist[1].deaths+careerlist[1].kills)..L" <icon=20189>IronBreaker: "..towstring(careerlist[1].kills)..L"/"..towstring(careerlist[1].deaths)..L" Score: "..towstring(careerlist[1].score))
	TextLogAddEntry("Chat", 21,careerlist[2].number..L" - "..towstring(careerlist[2].deaths+careerlist[2].kills)..L" <icon=20188>Slayer: "..towstring(careerlist[2].kills)..L"/"..towstring(careerlist[2].deaths)..L" Score: "..towstring(careerlist[2].score))
	TextLogAddEntry("Chat", 21,careerlist[3].number..L" - "..towstring(careerlist[3].deaths+careerlist[3].kills)..L" <icon=20193>RunePriest: "..towstring(careerlist[3].kills)..L"/"..towstring(careerlist[3].deaths)..L" Score: "..towstring(careerlist[3].score))
	TextLogAddEntry("Chat", 21,careerlist[4].number..L" - "..towstring(careerlist[4].deaths+careerlist[4].kills)..L" <icon=20187>Engineer: "..towstring(careerlist[4].kills)..L"/"..towstring(careerlist[4].deaths)..L" Score: "..towstring(careerlist[4].score))
	TextLogAddEntry("Chat", 21,careerlist[5].number..L" - "..towstring(careerlist[5].deaths+careerlist[5].kills)..L" <icon=20202>WichHunter: "..towstring(careerlist[5].kills)..L"/"..towstring(careerlist[5].deaths)..L" Score: "..towstring(careerlist[5].score))
	TextLogAddEntry("Chat", 21,careerlist[6].number..L" - "..towstring(careerlist[6].deaths+careerlist[6].kills)..L" <icon=20190>KoTBS: "..towstring(careerlist[6].kills)..L"/"..towstring(careerlist[6].deaths)..L" Score: "..towstring(careerlist[6].score))	
	TextLogAddEntry("Chat", 21,careerlist[7].number..L" - "..towstring(careerlist[7].deaths+careerlist[7].kills)..L" <icon=20183>BrightWizzard: "..towstring(careerlist[7].kills)..L"/"..towstring(careerlist[7].deaths)..L" Score: "..towstring(careerlist[7].score))	
	TextLogAddEntry("Chat", 21,careerlist[8].number..L" - "..towstring(careerlist[8].deaths+careerlist[8].kills)..L" <icon=20199>WarriorPriest: "..towstring(careerlist[8].kills)..L"/"..towstring(careerlist[8].deaths)..L" Score: "..towstring(careerlist[8].score))	
	TextLogAddEntry("Chat", 21,careerlist[9].number..L" - "..towstring(careerlist[9].deaths+careerlist[9].kills)..L" <icon=20198>SwordMaster: "..towstring(careerlist[9].kills)..L"/"..towstring(careerlist[9].deaths)..L" Score: "..towstring(careerlist[9].score))	
	TextLogAddEntry("Chat", 21,careerlist[10].number..L" - "..towstring(careerlist[10].deaths+careerlist[10].kills)..L" <icon=20194>ShadowWarrior: "..towstring(careerlist[10].kills)..L"/"..towstring(careerlist[10].deaths)..L" Score: "..towstring(careerlist[10].score))	
	TextLogAddEntry("Chat", 21,careerlist[11].number..L" - "..towstring(careerlist[11].deaths+careerlist[11].kills)..L" <icon=20200>WhiteLion: "..towstring(careerlist[11].kills)..L"/"..towstring(careerlist[11].deaths)..L" Score: "..towstring(careerlist[11].score))
	TextLogAddEntry("Chat", 21,careerlist[12].number..L" - "..towstring(careerlist[12].deaths+careerlist[12].kills)..L" <icon=20180>ArchMage: "..towstring(careerlist[12].kills)..L"/"..towstring(careerlist[12].deaths)..L" Score: "..towstring(careerlist[12].score))	
elseif (GameData.Player.realm == 1) then -- Class sorting for Destro kills
		TextLogAddEntry("Chat", 21,careerlist[0].number..L" - "..towstring(careerlist[0].deaths+careerlist[0].kills)..L" <icon=43>Unknown: "..towstring(careerlist[0].kills)..L"/"..towstring(careerlist[0].deaths)..L" Score: "..towstring(careerlist[0].score)..L" Ratio: "..wstring.format(L"%.01f",careerlist[0].ratio))
	TextLogAddEntry("Chat", 21,careerlist[1].number..L" - "..towstring(careerlist[1].deaths+careerlist[1].kills)..L" <icon=20182>Blackorc: "..towstring(careerlist[1].kills)..L"/"..towstring(careerlist[1].deaths)..L" Score: "..towstring(careerlist[1].score))
	TextLogAddEntry("Chat", 21,careerlist[2].number..L" - "..towstring(careerlist[2].deaths+careerlist[2].kills)..L" <icon=20184>Choppa: "..towstring(careerlist[2].kills)..L"/"..towstring(careerlist[2].deaths)..L" Score: "..towstring(careerlist[2].score))
	TextLogAddEntry("Chat", 21,careerlist[3].number..L" - "..towstring(careerlist[3].deaths+careerlist[3].kills)..L" <icon=20195>Shaman: "..towstring(careerlist[3].kills)..L"/"..towstring(careerlist[3].deaths)..L" Score: "..towstring(careerlist[3].score))
	TextLogAddEntry("Chat", 21,careerlist[4].number..L" - "..towstring(careerlist[4].deaths+careerlist[4].kills)..L" <icon=20197>SquigHerder: "..towstring(careerlist[4].kills)..L"/"..towstring(careerlist[4].deaths)..L" Score: "..towstring(careerlist[4].score))
	TextLogAddEntry("Chat", 21,careerlist[5].number..L" - "..towstring(careerlist[5].deaths+careerlist[5].kills)..L" <icon=20185>Chosen: "..towstring(careerlist[5].kills)..L"/"..towstring(careerlist[5].deaths)..L" Score: "..towstring(careerlist[5].score))
	TextLogAddEntry("Chat", 21,careerlist[6].number..L" - "..towstring(careerlist[6].deaths+careerlist[6].kills)..L" <icon=20192>Marauder: "..towstring(careerlist[6].kills)..L"/"..towstring(careerlist[6].deaths)..L" Score: "..towstring(careerlist[6].score))	
	TextLogAddEntry("Chat", 21,careerlist[7].number..L" - "..towstring(careerlist[7].deaths+careerlist[7].kills)..L" <icon=20203>Zealot: "..towstring(careerlist[7].kills)..L"/"..towstring(careerlist[7].deaths)..L" Score: "..towstring(careerlist[7].score))	
	TextLogAddEntry("Chat", 21,careerlist[8].number..L" - "..towstring(careerlist[8].deaths+careerlist[8].kills)..L" <icon=20191>Magus: "..towstring(careerlist[8].kills)..L"/"..towstring(careerlist[8].deaths)..L" Score: "..towstring(careerlist[8].score))	
	TextLogAddEntry("Chat", 21,careerlist[9].number..L" - "..towstring(careerlist[9].deaths+careerlist[9].kills)..L" <icon=20181>BlackGuard: "..towstring(careerlist[9].kills)..L"/"..towstring(careerlist[9].deaths)..L" Score: "..towstring(careerlist[9].score))	
	TextLogAddEntry("Chat", 21,careerlist[10].number..L" - "..towstring(careerlist[10].deaths+careerlist[10].kills)..L" <icon=20201>WichElf: "..towstring(careerlist[10].kills)..L"/"..towstring(careerlist[10].deaths)..L" Score: "..towstring(careerlist[10].score))	
	TextLogAddEntry("Chat", 21,careerlist[11].number..L" - "..towstring(careerlist[11].deaths+careerlist[11].kills)..L" <icon=20186>Deciple: "..towstring(careerlist[11].kills)..L"/"..towstring(careerlist[11].deaths)..L" Score: "..towstring(careerlist[11].score))
	TextLogAddEntry("Chat", 21,careerlist[12].number..L" - "..towstring(careerlist[12].deaths+careerlist[12].kills)..L" <icon=20196>Sorc: "..towstring(careerlist[12].kills)..L"/"..towstring(careerlist[12].deaths)..L" Score: "..towstring(careerlist[12].score))	
end	
TextLogAddEntry("Chat", 21,L"-----------------------------\n")
end


function sortlist(index,tablename)-- Sorting the tables
 --name
 if (Sorting == 0) then
    table.sort(index, function (a,b)
      return (a.Title < b.Title)
    end)
elseif (Sorting == 1) then
	 table.sort(index, function (a,b)
      return (a.Title > b.Title)
    end)
--kills
elseif (Sorting == 2) then
	 table.sort(index, function (a,b)
      return (a.kills < b.kills)
    end)	
elseif (Sorting == 3) then
	 table.sort(index, function (a,b)
      return (a.kills > b.kills)
    end)		
--deaths	
elseif (Sorting == 4) then
	 table.sort(index, function (a,b)
      return (a.deaths < b.deaths)
    end)		
elseif (Sorting == 5) then
	 table.sort(index, function (a,b)
      return (a.deaths > b.deaths)
    end)		
--score
	elseif (Sorting == 6) then
	 table.sort(index, function (a,b)
      return (a.score < b.score)
    end)		
elseif (Sorting == 7) then
	 table.sort(index, function (a,b)
      return (a.score > b.score)
    end)		
--icon
elseif (Sorting == 8) then
	 table.sort(index, function (a,b)
      return (a.icon < b.icon)
    end)		
elseif (Sorting == 9) then
	 table.sort(index, function (a,b)
      return (a.icon > b.icon)
    end)
	--rank
elseif (Sorting == 10) then
	 table.sort(index, function (a,b)
      return (a.ratio < b.ratio)
    end)	
elseif (Sorting == 11) then
	 table.sort(index, function (a,b)
      return (a.ratio > b.ratio)
    end)	
	
end

	ListBoxSetDisplayOrder(DB.winList, tablename)
--	filterDisplayOrder(DeathblowWindowList, _DisplayOrder)
end


function Deathblow.OnSelectProfil()
 local _index = ListBoxGetDataIndex(DB.winList, WindowGetId(SystemData.MouseOverWindow.name))
 
 --TextLogAddEntry("Chat", 21,L"Slasky "..string.find(SystemData.MouseOverWindow.name, "^" .. DB.winList .. "Row"))

 --TextLogAddEntry("Chat", 21,L"Slasky ".._index)

 end
-----------------------------------------
-- Tab Control
-----------------------------------------

function Deathblow.OnTabLBU()
local tabNumber	= WindowGetId (SystemData.ActiveWindow.name)

if (ListMode == 0) or (ListMode == 1) then

if string.find(SystemData.MouseOverWindow.name, "^" .. DB.winList .. "Row") then

local _index = ListBoxGetDataIndex(DB.winList, WindowGetId(SystemData.MouseOverWindow.name))
local Zlaske = {}	
local rowcount = 1
 --TextLogAddEntry("Chat", 21,L"Rownumber: ".._index) 
for k, v in pairs(DB.Listdata) do

Zlaske[rowcount] = DB.Listdata[k].Title
-- TextLogAddEntry("Chat", 21,L"Name: "..DB.Listdata[k].Title..L" Rownumber: "..rowcount)
 rowcount = rowcount+1

end
 ProfileName = Zlaske[_index]
ListMode = 4
tabNumber = 5
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",true)
Deathblow.Display()
 -- TextLogAddEntry("Chat", 21,L"Tada! "..Zlaske[_index]) 
	end
end -- en dif tab


if (tabNumber == 1) and (Deathblow.SavedSettings ~= nil) then 
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",true)
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",false)
ListMode = 0
Deathblow.Display()

elseif(tabNumber == 2) and (DBsession.list ~= nil) then 	
ListMode = 1
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",true)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",false)
ButtonSetDisabledFlag( "DeathblowWin1WindowSessionTab",false)
Deathblow.Display()
elseif(tabNumber == 3) and (Deathblow.SavedSettings ~= nil) then 

ListMode = 2
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",true)
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",false)
Deathblow.Display()

elseif(tabNumber == 4) and (Deathblow.SavedSettings ~= nil) then 

ListMode = 3
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",true)
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",false)
Deathblow.Display()
	
elseif(tabNumber == 5) and (ProfileName ~= nil) then 

ListMode = 4
ButtonSetPressedFlag( "DeathblowWin1WindowSummTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowClassTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowSessionTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowTotalTab",false)
ButtonSetPressedFlag( "DeathblowWin1WindowProfileTab",true)
Deathblow.Display()


--Settings

elseif(tabNumber == 19) then 
	WindowSetShowing("SettingsWindow", false)
	UpdateSettings()
	


elseif(tabNumber == 20) then 
	if DBSETTINGS.showspeach == 0 then 
		TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=58> KillSpeachBubble Hidden")	
		DBSETTINGS.showspeach = 1
		UpdateSettings()
	else 
		DBSETTINGS.showspeach = 0 
		TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=57> KillSpeachBubble Showing")
		UpdateSettings()
	end
	
elseif(tabNumber == 21) then 
	if DBSETTINGS.supresskill == 0 then 
		TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=58> KillAlerts Hidden")
		DBSETTINGS.supresskill = 1
		UpdateSettings()
	else 
		DBSETTINGS.supresskill = 0 
		TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=57> KillAlerts Showing")
		UpdateSettings()
	end


elseif(tabNumber == 22) then 
	if DBSETTINGS.supressdeath == 0 then 
		TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=58> DeathAlerts Hidden")
		DBSETTINGS.supressdeath = 1
		UpdateSettings()
	else 
		DBSETTINGS.supressdeath = 0 
		TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=57> DeathAlerts Showing")
		UpdateSettings()
	end
	
elseif(tabNumber == 23) then 
	if DBSETTINGS.supresstext == 0 then 
		TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=58> TextLog Hidden")
		DBSETTINGS.supresstext = 1
		UpdateSettings()
	else 
		DBSETTINGS.supresstext = 0 
		TextLogAddEntry("Chat", 0, L"[DeathBlow]: <icon=57> TextLog Showing")
		UpdateSettings()
	end	
	
	
elseif(tabNumber == 30) then 
	if WindowGetShowing("SettingsWindow") == false then
	WindowSetShowing("SettingsWindow", true)
	UpdateSettings()
	else 
	WindowSetShowing("SettingsWindow", false)
	UpdateSettings()
	end		
	
end
	
end

	



function UpdateSettings()
		if DBSETTINGS.showspeach == 0 then 
		ButtonSetPressedFlag( "SettingsWindowCheckBox1",true)			
		else 
		ButtonSetPressedFlag( "SettingsWindowCheckBox1",false)
		end

		if DBSETTINGS.supresskill == 0 then 
		ButtonSetPressedFlag( "SettingsWindowCheckBox2",true)			
		else 
		ButtonSetPressedFlag( "SettingsWindowCheckBox2",false)
		end	
	
		if DBSETTINGS.supressdeath == 0 then 
		ButtonSetPressedFlag( "SettingsWindowCheckBox3",true)			
		else 
		ButtonSetPressedFlag( "SettingsWindowCheckBox3",false)
		end		
	
		if DBSETTINGS.supresstext == 0 then 
		ButtonSetPressedFlag( "SettingsWindowCheckBox4",true)			
		else 
		ButtonSetPressedFlag( "SettingsWindowCheckBox4",false)
		end		
end

function Deathblow.SetupLanguage()

Language=SystemData.Settings.Language.active
	-----------------------------------------------------------------------------------------------------------------Spanish
	if(Language==5) then
txt_Title = L"Golpe mortal"
txt_OnKill1 = L"Usted "
txt_OnKill2 = L"Con"
txt_OnDeath1 = L"Tu eras "	
txt_OnDeath2 = L"por"	
txt_Score = L"Puntos"
txt_Kills = L"Matar"
txt_Deaths = L"Muerte"
txt_Ratio = L"Proporción"
txt_Total = L"Total"
txt_Enemys = L"Enemigos"

txt_Session = L"Sesión"
txt_Encounters = L"Encuentros"
txt_First_Kill = L"Primer Asesinato"
txt_First_Death = L"Primera Muerte"
txt_Latest_Kill = L"Últimas Kill"
txt_Latest_Death = L"Últimas Muerte"
txt_Level = L"Nivel "
txt_Careers = L"Carreras"
txt_Class = L"Clase"
txt_Unknown =L"Desconocido"
txt_None = L"Ninguna"
txt_Overall_Stats = L"Estadísticas Generales"
txt_Class_Stats = L"Clase Estadísticas"
txt_My = L"Mi "
txt_s = L" "

txt_Killspree = L"Matanza"
txt_Damage = L"Dañar"
txt_Mitigated = L"Mitigado"
txt_Criticalls = L"Críticamente"
txt_Dodged = L"Esquivado"
txt_Blocked = L"Bloqueado"
txt_Disrupted = L"Trastocado"
txt_Parried = L"Parar"
txt_Total_Damage = L"Total Daño"
txt_Total_Mitigated = L"Total Mitigada"
txt_Total_Crits = L"Total Crítica"
txt_Total_Dodges = L"Total Esquivado"
txt_Total_Blocks = L"Total Bloqueado"
txt_Total_Disrupts = L"Total Trastocado"
txt_Total_Parries = L"Total Parar"



--Static, no touchy
txt_dodged = L" esquivado "
txt_blocked = L" bloqueado "
txt_disrupted = L" trastocado "
txt_parried = L" parar "
txt_critically = L" críticamente "

txt_KotBS = L"KotBS"
TextLogAddEntry("Chat", 21,L"Language is: Spanish: "..Language)
	-----------------------------------------------------------------------------------------------------------------ITALIANO
	elseif(Language==4) then
txt_Title = L"Colpo mortale"
txt_OnDeath1 = L"Eri "
txt_OnDeath2 = L"di"
txt_OnKill1 = L"Tu "
txt_OnKill2 = L"con"
txt_Score = L"Punto"
txt_Kills = L"Uccide"
txt_Deaths = L"Morti"
txt_Ratio = L"?Ratio?"
txt_Total = L"Totale"
txt_Enemys = L"Nemici"

txt_Session = L"sessione"
txt_Encounters = L"Incontri"
txt_First_Kill = L"Prima Uccisione"
txt_First_Death = L"Prima Morte"
txt_Latest_Kill = L"Ultime Uccidere"
txt_Latest_Death = L"Ultime Morte"
txt_Level = L"Livello "
txt_Careers = L"Carriera"
txt_Class = L"Classe"
txt_Unknown =L"Sconosciuto"
txt_None = L"Nessuna"
txt_Overall_Stats = L"Statistica globale"
txt_Class_Stats = L"Classe Statistica"
txt_My = L"Il mio "
txt_s = L" "

txt_Killspree = L"Killspree"
txt_Damage = L"Damage"
txt_Mitigated = L"Mitigated"
txt_Criticalls = L"Criticalls"
txt_Dodged = L"Dodged"
txt_Blocked = L"Blocked"
txt_Disrupted = L"Disrupted"
txt_Parried = L"Parried"
txt_Total_Damage = L"Total Damage"
txt_Total_Mitigated = L"Total Mitigated"
txt_Total_Crits = L"Total Crits"
txt_Total_Dodges = L"Total Dodges"
txt_Total_Blocks = L"Total Blocks"
txt_Total_Disrupts = L"Total Disrupts"
txt_Total_Parries = L"Total Parries"



--Static, no touchy
txt_dodged = L" Schivato "
txt_blocked = L" Bloccato "
txt_disrupted = L" interrotto "
txt_parried = L" parato "
txt_critically = L"critici"

txt_KotBS = L"KotBS"
TextLogAddEntry("Chat", 21,L"Language is: Italian: "..Language)
	-----------------------------------------------------------------------------------------------------------------DEUTSCH
	elseif(Language==3) then
txt_Title = L"Todesstoß"
txt_OnKill1 = L"Du "
txt_OnKill2 = L"mit"
txt_OnDeath1 = L"Du wurdest "
txt_OnDeath2 = L"von"
txt_Score = L"Auswertung"
txt_Kills = L"Siege"
txt_Deaths = L"Tode"
txt_Ratio = L"Verhältnis"
txt_Total = L"Insgesamt"
txt_Enemys = L"Feinde"

txt_Session = L"Sitzung"
txt_Encounters = L"Gefechte"
txt_First_Kill = L"Erster Sieg"
txt_First_Death = L"Erster Tod"
txt_Latest_Kill = L"Letzter Sieg"
txt_Latest_Death = L"Letzter Tod"
txt_Level = L"Rang "
txt_Careers = L"Karriere"
txt_Class = L"Klasse"
txt_Unknown = L"Unbekannt"
txt_None = L"Keine"
txt_Overall_Stats =  L"Gesamte Attribute"
txt_Class_Stats =  L"Klassen Attribute" 
txt_My = L"Meine "
txt_s = L" "


txt_Killspree = L"Tötungsserie"
txt_Damage = L"Schaden"
txt_Mitigated = L"Abgeschwächt"
txt_Criticalls = L"Kritische" 
txt_Dodged = L"Ausgewichen"
txt_Blocked = L"Geblockt"
txt_Disrupted = L"Gestört"
txt_Parried = L"Pariert"
txt_Total_Damage = L"Gesamter<br>Schaden"
txt_Total_Mitigated = L"Gesamt<br>Abgeschwächt"
txt_Total_Crits = L"Gesamte<br>Kritische Treffer"
txt_Total_Dodges = L"Gesamt<br>Ausgewichen"
txt_Total_Blocks = L"Gesamt<br>Geblockt"
txt_Total_Disrupts = L"Gesamt<br>Gestört"
txt_Total_Parries = L"Gesamt<br>Pariert"




--Static, no touchy
txt_dodged = L" ausgewichen."
txt_blocked = L" geblockt."
txt_disrupted = L" gestört."
txt_parried = L" pariert."
txt_critically = L" kritisch "

txt_KotBS = L"KotBS"
TextLogAddEntry("Chat", 21,L"Language is: Deutsch: "..Language)
	-----------------------------------------------------------------------------------------------------------------FRANCAIS
	elseif(Language==2) then
txt_Title = L"Coup Mortel"
txt_OnKill1 = L"Toi "
txt_OnKill2 = L"avec"
txt_OnDeath1 = L"Tu as été"	
txt_OnDeath2 = L"par"	
txt_Score = L"Score"
txt_Kills = L"Kills"
txt_Deaths = L"Deaths"
txt_Ratio = L"Ratio"
txt_Total = L"Total"
txt_Enemys = L"Ennemis"

txt_Session = L"Session"
txt_Encounters = L"Affrontements"
txt_First_Kill = L"Fremier Kill"
txt_First_Death = L"Première Mort"
txt_Latest_Kill = L"Dernier Kill"
txt_Latest_Death = L"Dernière Mort"
txt_Level = L"Niveau "
txt_Careers = L"Carrières"
txt_Class = L"Classe"
txt_Unknown = L"Inconnu"
txt_None = L"Rien"
txt_Overall_Stats = L"Stats Globales"
txt_Class_Stats = L"Stats de Classe"
txt_My = L"Ma "
txt_s = L" "


txt_Killspree = L"Meurtre à la chaine"
txt_Damage = L"Dommages"
txt_Mitigated = L"Mitigés"
txt_Criticalls = L"Critiques"
txt_Dodged = L"Evités"
txt_Blocked = L"Bloqués"
txt_Disrupted = L"Interrompus"
txt_Parried = L"Parés"
txt_Total_Damage = L"Total des dommages"
txt_Total_Mitigated = L"Mitigation totale"
txt_Total_Crits = L"Total des critiques"
txt_Total_Dodges = L"Total<br>des esquives"
txt_Total_Blocks = L"Total<br>des blocages"
txt_Total_Disrupts = L"Total<br>des interruptions"
txt_Total_Parries = L"Total<br>des parades"


--Static, no touchy
txt_dodged = L" esquivé "
txt_blocked = L" bloqué "
txt_disrupted = L" dissipé "
txt_parried = L" paré "
txt_critically = L" critique "



txt_KotBS = L"KotBS"
TextLogAddEntry("Chat", 21,L"Language is: Francais: "..Language)
	-----------------------------------------------------------------------------------------------------------------ENGLISH
	elseif(Language==1) then
txt_Title = L"DeathBlow"
txt_OnKill1 = L"You "
txt_OnKill2 = L"with"
txt_OnDeath1 = L"You were "	
txt_OnDeath2 = L"by"	
txt_Score = L"Score"
txt_Kills = L"Kills"
txt_Deaths = L"Deaths"
txt_Ratio = L"Ratio"
txt_Total = L"Total"
txt_Enemys = L"Enemies"

txt_Session = L"Session"
txt_Encounters = L"Encounters"
txt_First_Kill = L"First Kill"
txt_First_Death = L"First Death"
txt_Latest_Kill = L"Latest Kill"
txt_Latest_Death = L"Latest Death"
txt_Level = L"Level "
txt_Careers = L"Careers"
txt_Class = L"Class"
txt_Unknown =L"Unknown"
txt_None = L"None"
txt_Overall_Stats = L"Overall Stats"
txt_Class_Stats = L"Class Stats"
txt_My = L"My "
txt_s = L"'s "

txt_Killspree = L"Killspree"
txt_Damage = L"Damage"
txt_Mitigated = L"Mitigated"
txt_Criticalls = L"Criticalls"
txt_Dodged = L"Dodged"
txt_Blocked = L"Blocked"
txt_Disrupted = L"Disrupted"
txt_Parried = L"Parried"
txt_Total_Damage = L"Total Damage"
txt_Total_Mitigated = L"Total Mitigated"
txt_Total_Crits = L"Total Crits"
txt_Total_Dodges = L"Total Dodges"
txt_Total_Blocks = L"Total Blocks"
txt_Total_Disrupts = L"Total Disrupts"
txt_Total_Parries = L"Total Parries"



--Static, no touchy
txt_dodged = L" dodged "
txt_blocked = L" blocked "
txt_disrupted = L" disrupted "
txt_parried = L" parried "
txt_critically = L" critically "





txt_KotBS = L"KotBS"
TextLogAddEntry("Chat", 21,L"Language is: English: "..Language)

-----------------------------------------------------------------------------------------------------------------Ruski
elseif(Language==10) then
txt_Title = L"Смертельный удар"
txt_OnKill1 = L"Вами "
txt_OnKill2 = L"при помощи"
txt_OnDeath1 = L"Вы были "	
txt_OnDeath2 = L"от руки"	
txt_Score = L"Счёт"
txt_Kills = L"Добиваний"
txt_Deaths = L"Смертей"
txt_Ratio = L"Соотношение"
txt_Total = L"Общий счёт"
txt_Enemys = L"Противников"

txt_Session = L"За сессию"
txt_Encounters = L"Схваток"
txt_First_Kill = L"Первая жертва"
txt_First_Death = L"Первая смерть"
txt_Latest_Kill = L"Недавняя жертва"
txt_Latest_Death = L"Недавняя смерть"
txt_Level = L"Уровень "
txt_Careers = L"Карьера"
txt_Class = L"Класс"
txt_Unknown =L"Неизвестно"
txt_None = L"Нет"
txt_Overall_Stats = L"Общая статистика"
txt_Class_Stats = L"Статистика по классам"
txt_My = L"Посредством "
txt_s = L" "

txt_Killspree = L"Серия убийств"
txt_Damage = L"Урон"
txt_Mitigated = L"Поглощено"
txt_Criticalls = L"Критические удары"
txt_Dodged = L"Уклонения"
txt_Blocked = L"Заблокировано"
txt_Disrupted = L"Несрабатываний"
txt_Parried = L"Паррировано"
txt_Total_Damage = L"Общий<br>урон"
txt_Total_Mitigated = L"Поглощено<br>урона"
txt_Total_Crits = L"Всего<br>критических ударов"
txt_Total_Dodges = L"Всего<br>уклонений"
txt_Total_Blocks = L"Блоков<br>щитом"
txt_Total_Disrupts = L"Всего<br>несрабатываний"
txt_Total_Parries = L"Отражено<br>ударов"



--Static, no touchy
txt_dodged = L" уклоняется."
txt_blocked = L" блок."
txt_disrupted = L" срабатывание."
txt_parried = L" парирует."
txt_critically = L" критического "

txt_KotBS = L"КоБС"
TextLogAddEntry("Chat", 21,L"Language is: Russian: "..Language)
end

end

function Deathblow.ToolTipMenuButton()
local text = txt_Title
    Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, text )
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_TOP)
end

--/script PlaySound(GameData.Sound.BETA_WARNING)